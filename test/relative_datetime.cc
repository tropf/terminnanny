// Copyright (C) 2021 Rolf Pfeffertal
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#include <terminnanny/relative_datetime.h>

using Catch::Matchers::Contains;
using Catch::Matchers::Equals;
using terminnanny::relative_datetime;

TEST_CASE("relative_datetime validity check") {
    relative_datetime r;
    REQUIRE(r.is_valid());

    SECTION("init") {
        r.year = "2002";
        REQUIRE(r.is_valid());
        r.month = "+12";
        REQUIRE(r.is_valid());
        r.day = "-8";
        REQUIRE(r.is_valid());
        r.hour = "00007";
        REQUIRE(r.is_valid());
        r.minute = "+90";
        REQUIRE(r.is_valid());
        r.second = "0";
        REQUIRE(r.is_valid());
    }

    SECTION("week is only relative") {
        r.week = "0";
        REQUIRE(!r.is_valid());
        r.week = "-0";
        REQUIRE(r.is_valid());
        r.week = "+0";
        REQUIRE(r.is_valid());
    }

    SECTION("day only relative if week is set") {
        r.day = "2";
        REQUIRE(r.is_valid());
        r.week = "-0";
        REQUIRE(!r.is_valid());
        r.day = "+5";
        REQUIRE(r.is_valid());
        r.day = "";
        r.week = "";
        REQUIRE(r.is_valid());
    }

    SECTION("invalid ranges") {
        r.year = "1899";
        REQUIRE(!r.is_valid());
        r.year = "1900";
        REQUIRE(r.is_valid());

        r.month = "13";
        REQUIRE(!r.is_valid());
        r.month = "12";
        REQUIRE(r.is_valid());
        r.month = "0";
        REQUIRE(!r.is_valid());
        r.month = "1";
        REQUIRE(r.is_valid());

        r.day = "32";
        REQUIRE(!r.is_valid());
        r.day = "31";
        REQUIRE(r.is_valid());
        r.day = "0";
        REQUIRE(!r.is_valid());
        r.day = "1";
        REQUIRE(r.is_valid());

        r.hour = "24";
        REQUIRE(!r.is_valid());
        r.hour = "23";
        REQUIRE(r.is_valid());
        r.hour = "0";
        REQUIRE(r.is_valid());

        r.minute = "60";
        REQUIRE(!r.is_valid());
        r.minute = "59";
        REQUIRE(r.is_valid());
        r.minute = "0";
        REQUIRE(r.is_valid());
        r.second = "60";
        REQUIRE(!r.is_valid());
        r.second = "59";
        REQUIRE(r.is_valid());
        r.second = "0";
        REQUIRE(r.is_valid());
    }

    SECTION("gibberish input") {
        r.second = "abc";
        REQUIRE(!r.is_valid());

        r.second = "+123.1";
        REQUIRE(!r.is_valid());

        r.second = " +3";
        REQUIRE(!r.is_valid());

        r.second = "1.1";
        REQUIRE(!r.is_valid());

        r.second = "+5ac";
        REQUIRE(!r.is_valid());

        r.second = " ";
        REQUIRE(!r.is_valid());

        r.second = "+";
        REQUIRE(!r.is_valid());

        r.second = "-";
        REQUIRE(!r.is_valid());

        r.second = "";
        REQUIRE(r.is_valid());
    }
}

TEST_CASE("relative_datetime only relative check") {
    relative_datetime r;
    REQUIRE(r.is_only_relative());

    r.year = "2000";
    REQUIRE(!r.is_only_relative());
    r.year = "+one hundred";
    REQUIRE(!r.is_only_relative());
    r.year = "-100";
    REQUIRE(r.is_only_relative());

    r.month = "1";
    REQUIRE(!r.is_only_relative());
    r.month = "xxx";
    REQUIRE(!r.is_only_relative());
    r.month = "-100";
    REQUIRE(r.is_only_relative());

    r.week = "13";
    REQUIRE(!r.is_only_relative());
    r.week = "+7";
    REQUIRE(r.is_only_relative());

    r.day = "2";
    REQUIRE(!r.is_only_relative());
    r.day = "-0";
    REQUIRE(r.is_only_relative());

    r.hour = "11";
    REQUIRE(!r.is_only_relative());
    r.hour = "-0";
    REQUIRE(r.is_only_relative());
    r.hour = "";
    REQUIRE(r.is_only_relative());

    r.minute = "7";
    REQUIRE(!r.is_only_relative());
    r.minute = "+7";
    REQUIRE(r.is_only_relative());

    r.second = "120";
    REQUIRE(!r.is_only_relative());
    r.second = "-120";
    REQUIRE(r.is_only_relative());
}

TEST_CASE("relative_datetime error to string") {
    relative_datetime::error e1 = {
        relative_datetime::error_field::field_month,
        relative_datetime::error_type::err_range
    };
    REQUIRE_THAT(e1.to_string(), !Contains("year"));
    REQUIRE_THAT(e1.to_string(), Contains("month"));
    REQUIRE_THAT(e1.to_string(), !Contains("second"));
    REQUIRE_THAT(e1.to_string(), Contains("range"));
    REQUIRE_THAT(e1.to_string(), !Contains("syntax"));
    REQUIRE_THAT(e1.to_string(), !Contains("relative"));
    REQUIRE_THAT(e1.to_string(), !Contains("unkown"));

    relative_datetime::error e2;
    REQUIRE_THAT(e2.to_string(), Equals(""));

    relative_datetime::error e3 = {
        relative_datetime::error_field::field_year,
        relative_datetime::error_type::err_only_relative
    };
    REQUIRE_THAT(e3.to_string(), Contains("year"));
    REQUIRE_THAT(e3.to_string(), !Contains("month"));
    REQUIRE_THAT(e3.to_string(), !Contains("second"));
    REQUIRE_THAT(e3.to_string(), !Contains("range"));
    REQUIRE_THAT(e3.to_string(), !Contains("syntax"));
    REQUIRE_THAT(e3.to_string(), Contains("relative"));
    REQUIRE_THAT(e3.to_string(), !Contains("unkown"));

    relative_datetime::error e4 = {
        relative_datetime::error_field::field_second,
        relative_datetime::error_type::err_syntax
    };
    REQUIRE_THAT(e4.to_string(), !Contains("year"));
    REQUIRE_THAT(e4.to_string(), !Contains("month"));
    REQUIRE_THAT(e4.to_string(), Contains("second"));
    REQUIRE_THAT(e4.to_string(), !Contains("range"));
    REQUIRE_THAT(e4.to_string(), Contains("syntax"));
    REQUIRE_THAT(e4.to_string(), !Contains("relative"));
    REQUIRE_THAT(e4.to_string(), !Contains("unkown"));

    relative_datetime::error e5 = {
        relative_datetime::error_field::field_none,
        relative_datetime::error_type::err_range
    };
    REQUIRE_THAT(e5.to_string(), !Contains("year"));
    REQUIRE_THAT(e5.to_string(), !Contains("month"));
    REQUIRE_THAT(e5.to_string(), !Contains("second"));
    REQUIRE_THAT(e5.to_string(), Contains("range"));
    REQUIRE_THAT(e5.to_string(), !Contains("syntax"));
    REQUIRE_THAT(e5.to_string(), !Contains("relative"));
    REQUIRE_THAT(e5.to_string(), Contains("unkown"));

    relative_datetime::error e6 = {
        relative_datetime::error_field::field_second,
        relative_datetime::error_type::err_ok
    };
    REQUIRE_THAT(e6.to_string(), !Contains("year"));
    REQUIRE_THAT(e6.to_string(), !Contains("month"));
    REQUIRE_THAT(e6.to_string(), Contains("second"));
    REQUIRE_THAT(e6.to_string(), !Contains("range"));
    REQUIRE_THAT(e6.to_string(), !Contains("syntax"));
    REQUIRE_THAT(e6.to_string(), !Contains("relative"));
    REQUIRE_THAT(e6.to_string(), Contains("no error"));
}

TEST_CASE("relative_datetime error strings") {
    relative_datetime r;
    REQUIRE(r.is_valid());

    r.year = " asjdk";
    REQUIRE(r.get_validation_error().field == relative_datetime::error_field::field_year);
    REQUIRE(r.get_validation_error().type == relative_datetime::error_type::err_syntax);
    r.year = "1899";
    REQUIRE(r.get_validation_error().field == relative_datetime::error_field::field_year);
    REQUIRE(r.get_validation_error().type == relative_datetime::error_type::err_range);
    r.year = "";
    REQUIRE(r.is_valid());

    r.month = "jhaks.1";
    REQUIRE(r.get_validation_error().field == relative_datetime::error_field::field_month);
    REQUIRE(r.get_validation_error().type == relative_datetime::error_type::err_syntax);
    r.month = "0";
    REQUIRE(r.get_validation_error().field == relative_datetime::error_field::field_month);
    REQUIRE(r.get_validation_error().type == relative_datetime::error_type::err_range);
    r.month = "";
    REQUIRE(r.is_valid());

    r.day = "jhaks.1";
    REQUIRE(r.get_validation_error().field == relative_datetime::error_field::field_day);
    REQUIRE(r.get_validation_error().type == relative_datetime::error_type::err_syntax);
    r.day = "32";
    REQUIRE(r.get_validation_error().field == relative_datetime::error_field::field_day);
    REQUIRE(r.get_validation_error().type == relative_datetime::error_type::err_range);
    r.day = "";
    REQUIRE(r.is_valid());

    r.hour = "jhaks.1";
    REQUIRE(r.get_validation_error().field == relative_datetime::error_field::field_hour);
    r.hour = "";
    REQUIRE(r.is_valid());

    r.minute = "jhaks.1";
    REQUIRE(r.get_validation_error().field == relative_datetime::error_field::field_minute);
    r.minute = "";
    REQUIRE(r.is_valid());

    r.second = "jhaks.1";
    REQUIRE(r.get_validation_error().field == relative_datetime::error_field::field_second);
    r.second = "";
    REQUIRE(r.is_valid());

    r.week = "jhaks.1";
    REQUIRE(r.get_validation_error().field == relative_datetime::error_field::field_week);
    REQUIRE(r.get_validation_error().type == relative_datetime::error_type::err_syntax);
    r.week = "3";
    REQUIRE(r.get_validation_error().field == relative_datetime::error_field::field_week);
    REQUIRE(r.get_validation_error().type == relative_datetime::error_type::err_only_relative);
    r.week = "";
}

TEST_CASE("relative_datetime operator==/!=") {
    relative_datetime r1, r2;

    REQUIRE(r1 == r2);
    REQUIRE(!(r1 != r2));
    r1.year = "abc";
    REQUIRE(r1 != r2);
    REQUIRE(!(r1 == r2));
    r2.year = "abc";
    REQUIRE(r1 == r2);
    REQUIRE(!(r1 != r2));

    REQUIRE(r1 == r2);
    REQUIRE(!(r1 != r2));
    r1.month = "abc";
    REQUIRE(r1 != r2);
    REQUIRE(!(r1 == r2));
    r2.month = "abc";
    REQUIRE(r1 == r2);
    REQUIRE(!(r1 != r2));

    REQUIRE(r1 == r2);
    REQUIRE(!(r1 != r2));
    r1.week = "abc";
    REQUIRE(r1 != r2);
    REQUIRE(!(r1 == r2));
    r2.week = "abc";
    REQUIRE(r1 == r2);
    REQUIRE(!(r1 != r2));

    REQUIRE(r1 == r2);
    REQUIRE(!(r1 != r2));
    r1.day = "abc";
    REQUIRE(r1 != r2);
    REQUIRE(!(r1 == r2));
    r2.day = "abc";
    REQUIRE(r1 == r2);
    REQUIRE(!(r1 != r2));

    REQUIRE(r1 == r2);
    REQUIRE(!(r1 != r2));
    r1.hour = "abc";
    REQUIRE(r1 != r2);
    REQUIRE(!(r1 == r2));
    r2.hour = "abc";
    REQUIRE(r1 == r2);
    REQUIRE(!(r1 != r2));

    REQUIRE(r1 == r2);
    REQUIRE(!(r1 != r2));
    r1.minute = "abc";
    REQUIRE(r1 != r2);
    REQUIRE(!(r1 == r2));
    r2.minute = "abc";
    REQUIRE(r1 == r2);
    REQUIRE(!(r1 != r2));

    REQUIRE(r1 == r2);
    REQUIRE(!(r1 != r2));
    r1.second = "abc";
    REQUIRE(r1 != r2);
    REQUIRE(!(r1 == r2));
    r2.second = "abc";
    REQUIRE(r1 == r2);
    REQUIRE(!(r1 != r2));

    REQUIRE(r1 == r2);
    REQUIRE(!(r1 != r2));
    r1.week_starts_monday = false;
    REQUIRE(r1 != r2);
    REQUIRE(!(r1 == r2));
    r2.week_starts_monday = false;
    REQUIRE(r1 == r2);
    REQUIRE(!(r1 != r2));
}

TEST_CASE("relative_datetime tidy") {
    relative_datetime r1_tidy = {"2002", "2", "-2", "", "", ""};
    relative_datetime r1_dirty = {"2002", "2", "-2", "3", "basjd", "77777"};

    r1_dirty.tidy();
    REQUIRE(r1_dirty == r1_tidy);
    r1_tidy.tidy();
    REQUIRE(r1_dirty == r1_tidy);

    relative_datetime r2_tidy = {"", "5", "", "", "17", "", "23", false};
    relative_datetime r2_dirty = {"", "5", "first", "", "17", "yx", "23", false};

    r2_dirty.tidy();
    REQUIRE(r2_dirty == r2_tidy);
    r2_tidy.tidy();
    REQUIRE(r2_dirty == r2_tidy);
    
    relative_datetime r3_tidy = {"", "", "", "-5"};
    relative_datetime r3_dirty = {"1899", "", "3", "-5"};

    r3_dirty.tidy();
    REQUIRE(r3_dirty == r3_tidy);
    r3_tidy.tidy();
    REQUIRE(r3_dirty == r3_tidy);
}

TEST_CASE("relative_datetime is_empty") {
    relative_datetime r;

    REQUIRE(r.is_empty());

    r.year = "absd";
    REQUIRE(!r.is_empty());
    r.year = "";
    REQUIRE(r.is_empty());

    r.month = "absd";
    REQUIRE(!r.is_empty());
    r.month = "";
    REQUIRE(r.is_empty());

    r.week = "absd";
    REQUIRE(!r.is_empty());
    r.week = "";
    REQUIRE(r.is_empty());

    r.day = "absd";
    REQUIRE(!r.is_empty());
    r.day = "";
    REQUIRE(r.is_empty());

    r.hour = "absd";
    REQUIRE(!r.is_empty());
    r.hour = "";
    REQUIRE(r.is_empty());

    r.minute = "absd";
    REQUIRE(!r.is_empty());
    r.minute = "";
    REQUIRE(r.is_empty());

    r.second = "absd";
    REQUIRE(!r.is_empty());
    r.second = "";
    REQUIRE(r.is_empty());
    
    r.week_starts_monday = true;
    REQUIRE(r.is_empty());
    r.week_starts_monday = false;
    REQUIRE(r.is_empty());
}

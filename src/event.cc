// Copyright (C) 2021 Rolf Pfeffertal
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#include <terminnanny/event.h>

#include <string>
#include <vector>
#include <set>
#include <algorithm>

#include <spdlog/spdlog.h>

#include <libical/ical.h>

#include <terminnanny/datetime.h>
#include <terminnanny/caldav.h>

using terminnanny::datetime;
using terminnanny::event;
using terminnanny::caldav;
using std::string;
using std::set;
using std::vector;

const datetime terminnanny::event::nulldatetime{0};

static void collect_starts(icalcomponent*, struct icaltime_span* span, void* data) {
    ((set<time_t>*) data)->insert(span->start);
}

set<event> terminnanny::event::from_ical(const string& ical_str, const datetime& from, const datetime& to) {
    if (from == to) {
        spdlog::warn("searching icals in timeframe of 0 width, results will probably be garbage");
    }
    
    icalcomponent* main_comp;
    main_comp = icalparser_parse_string(ical_str.c_str());
    if (nullptr == main_comp) {
        spdlog::warn("got empty ical");
        return {};
    }
    icalcomponent_normalize(main_comp);
    spdlog::debug("parsed ical");
    spdlog::debug("extracting {} events ical", icalcomponent_count_components(main_comp, ICAL_VEVENT_COMPONENT));

    set<event> events;

    for (icalcomponent* c = icalcomponent_get_first_component(main_comp, ICAL_VEVENT_COMPONENT);
         c != nullptr;
         c = icalcomponent_get_next_component(main_comp, ICAL_VEVENT_COMPONENT)
         ) {
        event e;
        const char* maybe_title = icalcomponent_get_summary(c);
        if (maybe_title) {
            e.title = maybe_title;
        }
        const char* maybe_description = icalcomponent_get_description(c);
        if (maybe_description) {
            e.description = maybe_description;
        }
        const char* maybe_location = icalcomponent_get_location(c);
        if (maybe_location) {
            e.location = maybe_location;
        }

        // might potentially find duplicates, so use a set
        set<time_t> starts_utc;
        bool rrules_left = true;
        spdlog::trace("found {} RRULEs", icalcomponent_count_properties(c, ICAL_RRULE_PROPERTY));
        while(rrules_left) {
            struct icaltimetype start = icaltime_from_string(from.get_string_utc(caldav::ical_date_format).c_str());
            struct icaltimetype end = icaltime_from_string(to.get_string_utc(caldav::ical_date_format).c_str());

            // despite calling foreach "recurrence": single events will also be found
            icalcomponent_foreach_recurrence(c, start, end, collect_starts, &starts_utc);

            icalproperty* prop = icalcomponent_get_first_property(c, ICAL_RRULE_PROPERTY);
            if (prop) {
                // run loop again, the lib only uses the first RRULE, so we remove it
                spdlog::trace("deleting prop: {}", icalproperty_as_ical_string(prop));
                icalcomponent_remove_property(c, prop);
            } else {
                rrules_left = false;
            }

        }
        spdlog::debug("found {} occurences in timespan", starts_utc.size());

        for (const auto& single_start_utc : starts_utc) {
            event single_event = e;
            single_event.start = datetime(single_start_utc);
            if (single_event.start > to || single_event.start < from) {
                spdlog::warn("extracted event with start {} outside boundaries ({} - {}), skipping", single_event.start.get_string_localtime(), from.get_string_localtime(), to.get_string_localtime());
                continue;
            }
            events.insert(single_event);
        }
    }

    icalcomponent_free(main_comp);

    return events;
}

set<event> terminnanny::event::from_icals(const vector<string>& icals, const datetime& from, const datetime& to) {
    set<event> all_events;

    for (const auto& single_ical : icals) {
        auto events_subset = from_ical(single_ical, from, to);
        spdlog::debug("extracted {} events from single ical", events_subset.size());
        all_events.insert(events_subset.begin(), events_subset.end());
    }
    
    return all_events;
}

bool terminnanny::operator<(const event& lhs, const event& rhs) {
    return lhs.start < rhs.start;
}

bool terminnanny::operator>(const event& lhs, const event& rhs) {
    return lhs.start > rhs.start;
}

bool terminnanny::operator<=(const event& lhs, const event& rhs) {
    return lhs.start <= rhs.start;
}

bool terminnanny::operator>=(const event& lhs, const event& rhs) {
    return lhs.start >= rhs.start;
}

bool terminnanny::operator==(const event& lhs, const event& rhs) {
    return lhs.start == rhs.start;
}

bool terminnanny::operator!=(const event& lhs, const event& rhs) {
    return lhs.start != rhs.start;
}

// Copyright (C) 2021 Rolf Pfeffertal
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#include <argp.h>
#include <cstdio>
#include <string>
#include <fstream>
#include <memory>

#include <spdlog/spdlog.h>

#include <terminnanny/cfg.h>

using Catch::Matchers::Contains;
using Catch::Matchers::Equals;
using terminnanny::config;
using std::string;

TEST_CASE("sensible defaults") {
    config c;
    // url should be empty -> error is thrown when checking
    REQUIRE(c.url.empty());

    // some things should be set
    REQUIRE(!c.format_event.empty());
    REQUIRE(!c.format_date.empty());
    REQUIRE(!c.format_time.empty());
    REQUIRE(!c.format_datetime.empty());
}

TEST_CASE("validation") {
    config c;
    REQUIRE(!c.is_valid());

    c.url = "https://example.com/";
    c.rel_delta.day = "+1";

    REQUIRE(c.is_valid());

    c.url = "";
    REQUIRE(!c.is_valid());
    REQUIRE_THAT(c.get_validation_error(), Contains("url"));
    c.url = "https://example.com/";
    REQUIRE(c.is_valid());

    c.auth_user = "abc";
    c.auth_pass = "718237";
    REQUIRE(c.is_valid());

    c.format_header = "u1823hj";
    c.format_event = "akjc";
    c.format_footer = "jakdb xny";
    c.format_empty = "nmyxc8782";
    c.format_date = "bnmyxc";
    c.format_time = "71823zghjxc";
    c.format_datetime = "189273hxjmchn";
    REQUIRE(c.is_valid());


    c.rel_start.day = "asbnc";
    REQUIRE(!c.is_valid());
    REQUIRE_THAT(c.get_validation_error(), Contains("start") && Contains("day") && Contains("syntax"));
    c.rel_start.day = "";
    REQUIRE(c.is_valid());

    c.rel_start.week = "1";
    REQUIRE(!c.is_valid());
    REQUIRE_THAT(c.get_validation_error(), Contains("start") && Contains("week") && Contains("relative"));
    c.rel_start.week = "";
    REQUIRE(c.is_valid());

    c.rel_delta.day = "";

    c.rel_end.hour = "77";
    REQUIRE(!c.is_valid());
    REQUIRE_THAT(c.get_validation_error(), Contains("end") && Contains("hour") && Contains("range"));
    c.rel_end.hour = "+5";
    REQUIRE(c.is_valid());

    // can only set delta or specify end directly
    c.rel_end.hour = "-10";
    c.rel_delta.minute = "+2";
    REQUIRE(!c.is_valid());
    REQUIRE_THAT(c.get_validation_error(), Contains("only") && Contains("end") && Contains("delta"));
    c.rel_end.hour = "";
    REQUIRE(c.is_valid());

    // end must be after start
    c.rel_delta.year = "-1";
    REQUIRE_THAT(c.get_validation_error(), Contains("start") && Contains("end"));
    REQUIRE(!c.is_valid());

    config c2;
    c2.url = "https://example.com/";
    c2.rel_end.day = "+1";
    c2.start_timestamp = "1610562036";
    REQUIRE(c2.is_valid());

    c2.end_timestamp = "1610562436";
    REQUIRE_THAT(c2.get_validation_error(), Contains("only") && Contains("timestamp"));
    REQUIRE(!c2.is_valid());

    c2.rel_end.day = "+1";
    REQUIRE_THAT(c2.get_validation_error(), Contains("only") && Contains("timestamp"));
    REQUIRE(!c2.is_valid());

    c2.end_timestamp = "";
    REQUIRE(c2.is_valid());

    c2.rel_start.minute = "+1";
    REQUIRE_THAT(c2.get_validation_error(), Contains("only") && Contains("timestamp"));
    REQUIRE(!c2.is_valid());

    config c3;
    c3.url = "scheme:anything";
    c3.start_timestamp = "1610502436";
    c3.end_timestamp = "1610562436";
    REQUIRE(c3.is_valid());

    // swapped
    c3.start_timestamp = "1610562436";
    c3.end_timestamp = "1610502436";
    REQUIRE_THAT(c3.get_validation_error(), Contains("start") && Contains("end"));
    REQUIRE(!c3.is_valid());

    c3.start_timestamp = "1610502436";
    c3.end_timestamp = "1610562436";
    REQUIRE(c3.is_valid());

    // is valid timestamp?
    c3.start_timestamp = "ajhskdbnyx";
    REQUIRE(!c3.is_valid());
    REQUIRE_THAT(c3.get_validation_error(), Contains("timestamp") || Contains("epoch"));
    c3.start_timestamp = "1610502436";
    REQUIRE(c3.is_valid());

    c3.end_timestamp = "ajhskdbnyx";
    REQUIRE(!c3.is_valid());
    REQUIRE_THAT(c3.get_validation_error(), Contains("timestamp") || Contains("epoch"));
    c3.end_timestamp = "1610502437";
    REQUIRE(c3.is_valid());
}

TEST_CASE("set via parse_single_opt") {
    // empty state, needed for api
    struct argp_state s;
    config c;

    REQUIRE("abc" != c.url);
    c.parse_single_opt(config::argp_options::url_option, "abc", &s);
    REQUIRE("abc" == c.url);

    spdlog::set_level(spdlog::level::trace);
    spdlog::trace("message to initialize loggers, please pass along.");
    spdlog::set_level(spdlog::level::info);

    c.parse_single_opt(config::argp_options::verbosity, "trace", &s);
    spdlog::trace("message to initialize loggers, please pass along.");
    REQUIRE(spdlog::level::trace == spdlog::get_level());
    c.parse_single_opt(config::argp_options::verbosity, "deBUg", &s);
    REQUIRE(spdlog::level::debug == spdlog::get_level());
    c.parse_single_opt(config::argp_options::verbosity, "INFO", &s);
    REQUIRE(spdlog::level::info == spdlog::get_level());
    c.parse_single_opt(config::argp_options::verbosity, "warN", &s);
    REQUIRE(spdlog::level::warn == spdlog::get_level());
    c.parse_single_opt(config::argp_options::verbosity, "err", &s);
    REQUIRE(spdlog::level::err == spdlog::get_level());
    c.parse_single_opt(config::argp_options::verbosity, "Critical", &s);
    REQUIRE(spdlog::level::critical == spdlog::get_level());

    c.parse_single_opt(config::argp_options::verbosity, "gibberish", &s);
    REQUIRE(spdlog::level::critical == spdlog::get_level());

    REQUIRE("aaa" != c.auth_user);
    c.parse_single_opt(config::argp_options::user, "aaa", &s);
    REQUIRE("aaa" == c.auth_user);

    REQUIRE("def" != c.auth_pass);
    c.parse_single_opt(config::argp_options::pass, "def", &s);
    REQUIRE("def" == c.auth_pass);

    REQUIRE("6789013" != c.rel_start.year);
    c.parse_single_opt(config::argp_options::from_year, "6789013", &s);
    REQUIRE("6789013" == c.rel_start.year);
    REQUIRE("ggg" != c.rel_start.month);
    c.parse_single_opt(config::argp_options::from_month, "ggg", &s);
    REQUIRE("ggg" == c.rel_start.month);
    REQUIRE("78192yxcn" != c.rel_start.week);
    c.parse_single_opt(config::argp_options::from_week, "78192yxcn", &s);
    REQUIRE("78192yxcn" == c.rel_start.week);
    REQUIRE("u189273ioajhyxkcl" != c.rel_start.day);
    c.parse_single_opt(config::argp_options::from_day, "u189273ioajhyxkcl", &s);
    REQUIRE("u189273ioajhyxkcl" == c.rel_start.day);
    REQUIRE("123878xc" != c.rel_start.hour);
    c.parse_single_opt(config::argp_options::from_hour, "123878xc", &s);
    REQUIRE("123878xc" == c.rel_start.hour);
    REQUIRE("jsk8u123123" != c.rel_start.minute);
    c.parse_single_opt(config::argp_options::from_minute, "jsk8u123123", &s);
    REQUIRE("jsk8u123123" == c.rel_start.minute);
    REQUIRE("1u8923 6as79cyxc" != c.rel_start.second);
    c.parse_single_opt(config::argp_options::from_second, "1u8923 6as79cyxc", &s);
    REQUIRE("1u8923 6as79cyxc" == c.rel_start.second);

    REQUIRE("eee6789013" != c.rel_end.year);
    c.parse_single_opt(config::argp_options::to_year, "eee6789013", &s);
    REQUIRE("eee6789013" == c.rel_end.year);
    REQUIRE("eeeggg" != c.rel_end.month);
    c.parse_single_opt(config::argp_options::to_month, "eeeggg", &s);
    REQUIRE("eeeggg" == c.rel_end.month);
    REQUIRE("eee78192yxcn" != c.rel_end.week);
    c.parse_single_opt(config::argp_options::to_week, "eee78192yxcn", &s);
    REQUIRE("eee78192yxcn" == c.rel_end.week);
    REQUIRE("eeeu189273ioajhyxkcl" != c.rel_end.day);
    c.parse_single_opt(config::argp_options::to_day, "eeeu189273ioajhyxkcl", &s);
    REQUIRE("eeeu189273ioajhyxkcl" == c.rel_end.day);
    REQUIRE("eee123878xc" != c.rel_end.hour);
    c.parse_single_opt(config::argp_options::to_hour, "eee123878xc", &s);
    REQUIRE("eee123878xc" == c.rel_end.hour);
    REQUIRE("eeejsk8u123123" != c.rel_end.minute);
    c.parse_single_opt(config::argp_options::to_minute, "eeejsk8u123123", &s);
    REQUIRE("eeejsk8u123123" == c.rel_end.minute);
    REQUIRE("eee1u8923 6as79cyxc" != c.rel_end.second);
    c.parse_single_opt(config::argp_options::to_second, "eee1u8923 6as79cyxc", &s);
    REQUIRE("eee1u8923 6as79cyxc" == c.rel_end.second);

    REQUIRE("ddd6789013" != c.rel_delta.year);
    c.parse_single_opt(config::argp_options::delta_year, "ddd6789013", &s);
    REQUIRE("ddd6789013" == c.rel_delta.year);
    REQUIRE("dddggg" != c.rel_delta.month);
    c.parse_single_opt(config::argp_options::delta_month, "dddggg", &s);
    REQUIRE("dddggg" == c.rel_delta.month);
    REQUIRE("ddd78192yxcn" != c.rel_delta.week);
    c.parse_single_opt(config::argp_options::delta_week, "ddd78192yxcn", &s);
    REQUIRE("ddd78192yxcn" == c.rel_delta.week);
    REQUIRE("dddu189273ioajhyxkcl" != c.rel_delta.day);
    c.parse_single_opt(config::argp_options::delta_day, "dddu189273ioajhyxkcl", &s);
    REQUIRE("dddu189273ioajhyxkcl" == c.rel_delta.day);
    REQUIRE("ddd123878xc" != c.rel_delta.hour);
    c.parse_single_opt(config::argp_options::delta_hour, "ddd123878xc", &s);
    REQUIRE("ddd123878xc" == c.rel_delta.hour);
    REQUIRE("dddjsk8u123123" != c.rel_delta.minute);
    c.parse_single_opt(config::argp_options::delta_minute, "dddjsk8u123123", &s);
    REQUIRE("dddjsk8u123123" == c.rel_delta.minute);
    REQUIRE("ddd1u8923 6as79cyxc" != c.rel_delta.second);
    c.parse_single_opt(config::argp_options::delta_second, "ddd1u8923 6as79cyxc", &s);
    REQUIRE("ddd1u8923 6as79cyxc" == c.rel_delta.second);

    c.parse_single_opt(config::argp_options::week_start_sunday, nullptr, &s);
    REQUIRE(!c.rel_start.week_starts_monday);
    REQUIRE(!c.rel_end.week_starts_monday);
    REQUIRE(!c.rel_delta.week_starts_monday);

    c.parse_single_opt(config::argp_options::week_start_monday, nullptr, &s);
    REQUIRE(c.rel_start.week_starts_monday);
    REQUIRE(c.rel_end.week_starts_monday);
    REQUIRE(c.rel_delta.week_starts_monday);

    REQUIRE("asbcyxcjhyxc" != c.format_header);
    c.parse_single_opt(config::argp_options::format_header_opt, "asbcyxcjhyxc", &s);
    REQUIRE("asbcyxcjhyxc" == c.format_header);
    REQUIRE("71823789xcjhk" != c.format_event);
    c.parse_single_opt(config::argp_options::format_event_opt, "71823789xcjhk", &s);
    REQUIRE("71823789xcjhk" == c.format_event);
    REQUIRE("1273xc67yxc781231" != c.format_footer);
    c.parse_single_opt(config::argp_options::format_footer_opt, "1273xc67yxc781231", &s);
    REQUIRE("1273xc67yxc781231" == c.format_footer);
    REQUIRE("knatata" != c.format_empty);
    c.parse_single_opt(config::argp_options::format_empty_opt, "knatata", &s);
    REQUIRE("knatata" == c.format_empty);
    REQUIRE("18927389xcyyxc" != c.format_date);
    c.parse_single_opt(config::argp_options::format_date_opt, "18927389xcyyxc", &s);
    REQUIRE("18927389xcyyxc" == c.format_date);
    REQUIRE("189237yxc" != c.format_time);
    c.parse_single_opt(config::argp_options::format_time_opt, "189237yxc", &s);
    REQUIRE("189237yxc" == c.format_time);
    REQUIRE("89127yxcnnn" != c.format_datetime);
    c.parse_single_opt(config::argp_options::format_datetime_opt, "89127yxcnnn", &s);
    REQUIRE("89127yxcnnn" == c.format_datetime);

    REQUIRE("127378789s7878789" != c.start_timestamp);
    c.parse_single_opt(config::argp_options::from_epoch, "127378789s7878789", &s);
    REQUIRE("127378789s7878789" == c.start_timestamp);
    REQUIRE("12smcnyxmc" != c.end_timestamp);
    c.parse_single_opt(config::argp_options::to_epoch, "12smcnyxmc", &s);
    REQUIRE("12smcnyxmc" == c.end_timestamp);

    // check that the options have not been touchd in the meantime
    REQUIRE("abc" == c.url);
    REQUIRE(spdlog::level::critical == spdlog::get_level());
    REQUIRE("aaa" == c.auth_user);
    REQUIRE("def" == c.auth_pass);

    REQUIRE("6789013" == c.rel_start.year);
    REQUIRE("ggg" == c.rel_start.month);
    REQUIRE("78192yxcn" == c.rel_start.week);
    REQUIRE("u189273ioajhyxkcl" == c.rel_start.day);
    REQUIRE("123878xc" == c.rel_start.hour);
    REQUIRE("jsk8u123123" == c.rel_start.minute);
    REQUIRE("1u8923 6as79cyxc" == c.rel_start.second);

    REQUIRE("eee6789013" == c.rel_end.year);
    REQUIRE("eeeggg" == c.rel_end.month);
    REQUIRE("eee78192yxcn" == c.rel_end.week);
    REQUIRE("eeeu189273ioajhyxkcl" == c.rel_end.day);
    REQUIRE("eee123878xc" == c.rel_end.hour);
    REQUIRE("eeejsk8u123123" == c.rel_end.minute);
    REQUIRE("eee1u8923 6as79cyxc" == c.rel_end.second);

    REQUIRE("ddd6789013" == c.rel_delta.year);
    REQUIRE("dddggg" == c.rel_delta.month);
    REQUIRE("ddd78192yxcn" == c.rel_delta.week);
    REQUIRE("dddu189273ioajhyxkcl" == c.rel_delta.day);
    REQUIRE("ddd123878xc" == c.rel_delta.hour);
    REQUIRE("dddjsk8u123123" == c.rel_delta.minute);
    REQUIRE("ddd1u8923 6as79cyxc" == c.rel_delta.second);

    REQUIRE(c.rel_start.week_starts_monday);
    REQUIRE(c.rel_end.week_starts_monday);
    REQUIRE(c.rel_delta.week_starts_monday);

    REQUIRE("asbcyxcjhyxc" == c.format_header);
    REQUIRE("71823789xcjhk" == c.format_event);
    REQUIRE("1273xc67yxc781231" == c.format_footer);
    REQUIRE("knatata" == c.format_empty);
    REQUIRE("18927389xcyyxc" == c.format_date);
    REQUIRE("189237yxc" == c.format_time);
    REQUIRE("89127yxcnnn" == c.format_datetime);

    REQUIRE("127378789s7878789" == c.start_timestamp);
    REQUIRE("12smcnyxmc" == c.end_timestamp);
}

TEST_CASE("set parse_args") {
    // to create string pointers on stack
    char s00[] = "bin-name";
    char s01[] = "--url";
    char s02[] = "https://example.com/caldav";
    char s03[] = "--week-start-sunday";
    char s04[] = "--from-year";
    char s05[] = "x712381";
    char* args_simple[] = {s00, s01, s02, s03, s04, s05};
    config c_simple;
    REQUIRE(c_simple.parse_args(sizeof(args_simple) / sizeof(args_simple[0]), args_simple));

    char s10[] = "bin-name";
    char s11[] = "--help";
    char* args_help[] = {s10, s11};
    // expect exit after help
    config c_help;
    REQUIRE(!c_help.parse_args(sizeof(args_help) / sizeof(args_help[0]), args_help, ARGP_SILENT | ARGP_NO_EXIT));

    char s20[] = "bin-name";
    char* args_none[] = {s20};
    config c_none;
    REQUIRE(c_none.parse_args(sizeof(args_none) / sizeof(args_none[0]), args_none));

    // should refuse parsing
    config c_null;
    REQUIRE(!c_null.parse_args(0, {}));

    // unkown arguments
    char s30[] = "bin-name";
    char s31[] = "--unkown-argmunetjas";
    char* args_invalid[] = {s30, s31};
    config c_invalid;
    REQUIRE(!c_invalid.parse_args(sizeof(args_invalid) / sizeof(args_invalid[0]), args_invalid, ARGP_SILENT | ARGP_NO_EXIT));

    // argp flags are passed through (1)
    char s40[] = "bin-name";
    char s41[] = "-format-event";
    char s42[] = "xxx";
    char* args_flags[] = {s40, s41, s42};
    config c_flags1;
    // would not normally work, -format-event has only a single dash
    REQUIRE(c_flags1.parse_args(sizeof(args_flags) / sizeof(args_flags[0]), args_flags, ARGP_LONG_ONLY));
    REQUIRE("xxx" == c_flags1.format_event);

    // argp flags are passed through (2)
    // copy again, as argp consumes argv, so fresh data is needed
    char s50[] = "bin-name";
    char s51[] = "-format-event";
    char s52[] = "xxx";
    char* args_flags_copy[] = {s50, s51, s52};
    config c_flags2;
    c_flags2.try_parse_args(sizeof(args_flags_copy) / sizeof(args_flags_copy[0]), args_flags_copy, ARGP_LONG_ONLY);
    REQUIRE("xxx" == c_flags2.format_event);

    // extra arguments throw error
    char s60[] = "bin-name";
    char s61[] = "positional argument";
    char* args_args[] = {s60, s61};
    config c_args;
    REQUIRE(!c_args.parse_args(sizeof(args_args) / sizeof(args_args[0]), args_args, ARGP_SILENT));
    
    char opt00[] = "bin-name";
    char opt01[] = "--pass";
    char opt02[] = "def";
    char opt03[] = "-u";
    char opt04[] = "abc";
    char opt05[] = "--user";
    char opt06[] = "aaa";
    char opt07[] = "--delta-day";
    char opt08[] = "dddu189273ioajhyxkcl";
    char opt09[] = "--delta-hour";
    char opt10[] = "ddd123878xc";
    char opt11[] = "--delta-minute";
    char opt12[] = "dddjsk8u123123";
    char opt13[] = "--delta-month";
    char opt14[] = "dddggg";
    char opt15[] = "--delta-second";
    char opt16[] = "ddd1u8923 6as79cyxc";
    char opt17[] = "--delta-week";
    char opt18[] = "ddd78192yxcn";
    char opt19[] = "--delta-year";
    char opt20[] = "ddd6789013";
    char opt21[] = "--from-day";
    char opt22[] = "u189273ioajhyxkcl";
    char opt23[] = "--from-hour";
    char opt24[] = "123878xc";
    char opt25[] = "--from-minute";
    char opt26[] = "jsk8u123123";
    char opt27[] = "--from-month";
    char opt28[] = "ggg";
    char opt29[] = "--from-second";
    char opt30[] = "1u8923 6as79cyxc";
    char opt31[] = "--from-week";
    char opt32[] = "78192yxcn";
    char opt33[] = "--from-year";
    char opt34[] = "6789013";
    char opt35[] = "--to-day";
    char opt36[] = "eeeu189273ioajhyxkcl";
    char opt37[] = "--to-hour";
    char opt38[] = "eee123878xc";
    char opt39[] = "--to-minute";
    char opt40[] = "eeejsk8u123123";
    char opt41[] = "--to-month";
    char opt42[] = "eeeggg";
    char opt43[] = "--to-second";
    char opt44[] = "eee1u8923 6as79cyxc";
    char opt45[] = "--to-week";
    char opt46[] = "eee78192yxcn";
    char opt47[] = "--to-year";
    char opt48[] = "eee6789013";
    char opt49[] = "--format-date";
    char opt50[] = "18927389xcyyxc";
    char opt51[] = "--format-datetime";
    char opt52[] = "89127yxcnnn";
    char opt53[] = "--format-empty";
    char opt54[] = "knatata";
    char opt55[] = "--format-event";
    char opt56[] = "71823789xcjhk";
    char opt57[] = "--format-footer";
    char opt58[] = "1273xc67yxc781231";
    char opt59[] = "--format-header";
    char opt60[] = "asbcyxcjhyxc";
    char opt61[] = "--format-time";
    char opt62[] = "189237yxc";
    char opt63[] = "--verbosity";
    char opt64[] = "eRr";
    char opt65[] = "--from-epoch";
    char opt66[] = "127378789s7878789";
    char opt67[] = "--to-epoch";
    char opt68[] = "12smcnyxmc";
            
    char* args_complicated[] = {opt00, opt01, opt02, opt03, opt04, opt05, opt06, opt07, opt08, opt09, opt10, opt11, opt12, opt13, opt14, opt15, opt16, opt17, opt18, opt19, opt20, opt21, opt22, opt23, opt24, opt25, opt26, opt27, opt28, opt29, opt30, opt31, opt32, opt33, opt34, opt35, opt36, opt37, opt38, opt39, opt40, opt41, opt42, opt43, opt44, opt45, opt46, opt47, opt48, opt49, opt50, opt51, opt52, opt53, opt54, opt55, opt56, opt57, opt58, opt59, opt60, opt61, opt62, opt63, opt64, opt65, opt66, opt67, opt68};
    config c;
    // no parse errors, but will still be invalid
    REQUIRE(c.parse_args(sizeof(args_complicated) / sizeof(args_complicated[0]), args_complicated));

    REQUIRE("6789013" == c.rel_start.year);
    REQUIRE("ggg" == c.rel_start.month);
    REQUIRE("78192yxcn" == c.rel_start.week);
    REQUIRE("u189273ioajhyxkcl" == c.rel_start.day);
    REQUIRE("123878xc" == c.rel_start.hour);
    REQUIRE("jsk8u123123" == c.rel_start.minute);
    REQUIRE("1u8923 6as79cyxc" == c.rel_start.second);

    REQUIRE("eee6789013" == c.rel_end.year);
    REQUIRE("eeeggg" == c.rel_end.month);
    REQUIRE("eee78192yxcn" == c.rel_end.week);
    REQUIRE("eeeu189273ioajhyxkcl" == c.rel_end.day);
    REQUIRE("eee123878xc" == c.rel_end.hour);
    REQUIRE("eeejsk8u123123" == c.rel_end.minute);
    REQUIRE("eee1u8923 6as79cyxc" == c.rel_end.second);

    REQUIRE("ddd6789013" == c.rel_delta.year);
    REQUIRE("dddggg" == c.rel_delta.month);
    REQUIRE("ddd78192yxcn" == c.rel_delta.week);
    REQUIRE("dddu189273ioajhyxkcl" == c.rel_delta.day);
    REQUIRE("ddd123878xc" == c.rel_delta.hour);
    REQUIRE("dddjsk8u123123" == c.rel_delta.minute);
    REQUIRE("ddd1u8923 6as79cyxc" == c.rel_delta.second);

    REQUIRE(c.rel_start.week_starts_monday);
    REQUIRE(c.rel_end.week_starts_monday);
    REQUIRE(c.rel_delta.week_starts_monday);

    REQUIRE("asbcyxcjhyxc" == c.format_header);
    REQUIRE("71823789xcjhk" == c.format_event);
    REQUIRE("1273xc67yxc781231" == c.format_footer);
    REQUIRE("knatata" == c.format_empty);
    REQUIRE("18927389xcyyxc" == c.format_date);
    REQUIRE("189237yxc" == c.format_time);
    REQUIRE("89127yxcnnn" == c.format_datetime);

    REQUIRE("127378789s7878789" == c.start_timestamp);
    REQUIRE("12smcnyxmc" == c.end_timestamp);
}

TEST_CASE("set via parse_line") {
    config c1;
    REQUIRE(!c1.parse_line("# this is a comment"));
    REQUIRE(!c1.parse_line(""));
    REQUIRE(!c1.parse_line("   "));
    REQUIRE(!c1.parse_line("unkown-key"));

    REQUIRE(c1.parse_line("   url      askjd ajsdk"));
    REQUIRE("askjd ajsdk" == c1.url);
    // arg missing -> set to ""
    REQUIRE(c1.parse_line("    url          "));
    REQUIRE("" == c1.url);

    REQUIRE(c1.parse_line("               week-start-sunday   "));
    REQUIRE(!c1.rel_end.week_starts_monday);
    // ignore unnecessary args
    REQUIRE(c1.parse_line("               week-start-monday   asd"));
    REQUIRE(c1.rel_end.week_starts_monday);

    REQUIRE(c1.parse_line("format-header    abcdef"));
    REQUIRE("abcdef" == c1.format_header);

    config c;

    REQUIRE("abc" != c.url);
    REQUIRE(c.parse_line("url abc"));
    REQUIRE("abc" == c.url);


    spdlog::set_level(spdlog::level::trace);
    spdlog::trace("message to initialize loggers, please pass along.");
    spdlog::set_level(spdlog::level::info);

    REQUIRE(spdlog::level::info == spdlog::get_level());
    REQUIRE(c.parse_line("verbosity warN"));
    REQUIRE(!c.parse_line("verbosity not a loglevel"));
    REQUIRE(spdlog::level::warn == spdlog::get_level());

    REQUIRE("aaa" != c.auth_user);
    REQUIRE(c.parse_line("user aaa"));
    REQUIRE("aaa" == c.auth_user);

    REQUIRE("def" != c.auth_pass);
    REQUIRE(c.parse_line("pass def"));
    REQUIRE("def" == c.auth_pass);

    REQUIRE("6789013" != c.rel_start.year);
    REQUIRE(c.parse_line("from-year 6789013"));
    REQUIRE("6789013" == c.rel_start.year);
    REQUIRE("ggg" != c.rel_start.month);
    REQUIRE(c.parse_line("from-month ggg"));
    REQUIRE("ggg" == c.rel_start.month);
    REQUIRE("78192yxcn" != c.rel_start.week);
    REQUIRE(c.parse_line("from-week 78192yxcn"));
    REQUIRE("78192yxcn" == c.rel_start.week);
    REQUIRE("u189273ioajhyxkcl" != c.rel_start.day);
    REQUIRE(c.parse_line("from-day u189273ioajhyxkcl"));
    REQUIRE("u189273ioajhyxkcl" == c.rel_start.day);
    REQUIRE("123878xc" != c.rel_start.hour);
    REQUIRE(c.parse_line("from-hour 123878xc"));
    REQUIRE("123878xc" == c.rel_start.hour);
    REQUIRE("jsk8u123123" != c.rel_start.minute);
    REQUIRE(c.parse_line("from-minute jsk8u123123"));
    REQUIRE("jsk8u123123" == c.rel_start.minute);
    REQUIRE("1u8923 6as79cyxc" != c.rel_start.second);
    REQUIRE(c.parse_line("from-second 1u8923 6as79cyxc"));
    REQUIRE("1u8923 6as79cyxc" == c.rel_start.second);

    REQUIRE("eee6789013" != c.rel_end.year);
    REQUIRE(c.parse_line("to-year eee6789013"));
    REQUIRE("eee6789013" == c.rel_end.year);
    REQUIRE("eeeggg" != c.rel_end.month);
    REQUIRE(c.parse_line("to-month eeeggg"));
    REQUIRE("eeeggg" == c.rel_end.month);
    REQUIRE("eee78192yxcn" != c.rel_end.week);
    REQUIRE(c.parse_line("to-week eee78192yxcn"));
    REQUIRE("eee78192yxcn" == c.rel_end.week);
    REQUIRE("eeeu189273ioajhyxkcl" != c.rel_end.day);
    REQUIRE(c.parse_line("to-day eeeu189273ioajhyxkcl"));
    REQUIRE("eeeu189273ioajhyxkcl" == c.rel_end.day);
    REQUIRE("eee123878xc" != c.rel_end.hour);
    REQUIRE(c.parse_line("to-hour eee123878xc"));
    REQUIRE("eee123878xc" == c.rel_end.hour);
    REQUIRE("eeejsk8u123123" != c.rel_end.minute);
    REQUIRE(c.parse_line("to-minute eeejsk8u123123"));
    REQUIRE("eeejsk8u123123" == c.rel_end.minute);
    REQUIRE("eee1u8923 6as79cyxc" != c.rel_end.second);
    REQUIRE(c.parse_line("to-second eee1u8923 6as79cyxc"));
    REQUIRE("eee1u8923 6as79cyxc" == c.rel_end.second);

    REQUIRE("ddd6789013" != c.rel_delta.year);
    REQUIRE(c.parse_line("delta-year ddd6789013"));
    REQUIRE("ddd6789013" == c.rel_delta.year);
    REQUIRE("dddggg" != c.rel_delta.month);
    REQUIRE(c.parse_line("delta-month dddggg"));
    REQUIRE("dddggg" == c.rel_delta.month);
    REQUIRE("ddd78192yxcn" != c.rel_delta.week);
    REQUIRE(c.parse_line("delta-week ddd78192yxcn"));
    REQUIRE("ddd78192yxcn" == c.rel_delta.week);
    REQUIRE("dddu189273ioajhyxkcl" != c.rel_delta.day);
    REQUIRE(c.parse_line("delta-day dddu189273ioajhyxkcl"));
    REQUIRE("dddu189273ioajhyxkcl" == c.rel_delta.day);
    REQUIRE("ddd123878xc" != c.rel_delta.hour);
    REQUIRE(c.parse_line("delta-hour ddd123878xc"));
    REQUIRE("ddd123878xc" == c.rel_delta.hour);
    REQUIRE("dddjsk8u123123" != c.rel_delta.minute);
    REQUIRE(c.parse_line("delta-minute dddjsk8u123123"));
    REQUIRE("dddjsk8u123123" == c.rel_delta.minute);
    REQUIRE("ddd1u8923 6as79cyxc" != c.rel_delta.second);
    REQUIRE(c.parse_line("delta-second ddd1u8923 6as79cyxc"));
    REQUIRE("ddd1u8923 6as79cyxc" == c.rel_delta.second);

    REQUIRE(c.parse_line("week-start-sunday"));
    REQUIRE(!c.rel_start.week_starts_monday);
    REQUIRE(!c.rel_end.week_starts_monday);
    REQUIRE(!c.rel_delta.week_starts_monday);

    REQUIRE(c.parse_line("week-start-monday"));
    REQUIRE(c.rel_start.week_starts_monday);
    REQUIRE(c.rel_end.week_starts_monday);
    REQUIRE(c.rel_delta.week_starts_monday);

    REQUIRE("asbcyxcjhyxc" != c.format_header);
    REQUIRE(c.parse_line("format-header asbcyxcjhyxc"));
    REQUIRE("asbcyxcjhyxc" == c.format_header);
    REQUIRE("71823789xcjhk" != c.format_event);
    REQUIRE(c.parse_line("format-event 71823789xcjhk"));
    REQUIRE("71823789xcjhk" == c.format_event);
    REQUIRE("1273xc67yxc781231" != c.format_footer);
    REQUIRE(c.parse_line("format-footer 1273xc67yxc781231"));
    REQUIRE("1273xc67yxc781231" == c.format_footer);
    REQUIRE("knatata" != c.format_empty);
    REQUIRE(c.parse_line("format-empty knatata"));
    REQUIRE("knatata" == c.format_empty);
    REQUIRE("18927389xcyyxc" != c.format_date);
    REQUIRE(c.parse_line("format-date 18927389xcyyxc"));
    REQUIRE("18927389xcyyxc" == c.format_date);
    REQUIRE("189237yxc" != c.format_time);
    REQUIRE(c.parse_line("format-time 189237yxc"));
    REQUIRE("189237yxc" == c.format_time);
    REQUIRE("89127yxcnnn" != c.format_datetime);
    REQUIRE(c.parse_line("format-datetime 89127yxcnnn"));
    REQUIRE("89127yxcnnn" == c.format_datetime);

    // check that the options have not been touchd in the meantime
    REQUIRE("abc" == c.url);
    REQUIRE(spdlog::level::warn == spdlog::get_level());
    REQUIRE("aaa" == c.auth_user);
    REQUIRE("def" == c.auth_pass);

    REQUIRE("6789013" == c.rel_start.year);
    REQUIRE("ggg" == c.rel_start.month);
    REQUIRE("78192yxcn" == c.rel_start.week);
    REQUIRE("u189273ioajhyxkcl" == c.rel_start.day);
    REQUIRE("123878xc" == c.rel_start.hour);
    REQUIRE("jsk8u123123" == c.rel_start.minute);
    REQUIRE("1u8923 6as79cyxc" == c.rel_start.second);

    REQUIRE("eee6789013" == c.rel_end.year);
    REQUIRE("eeeggg" == c.rel_end.month);
    REQUIRE("eee78192yxcn" == c.rel_end.week);
    REQUIRE("eeeu189273ioajhyxkcl" == c.rel_end.day);
    REQUIRE("eee123878xc" == c.rel_end.hour);
    REQUIRE("eeejsk8u123123" == c.rel_end.minute);
    REQUIRE("eee1u8923 6as79cyxc" == c.rel_end.second);

    REQUIRE("ddd6789013" == c.rel_delta.year);
    REQUIRE("dddggg" == c.rel_delta.month);
    REQUIRE("ddd78192yxcn" == c.rel_delta.week);
    REQUIRE("dddu189273ioajhyxkcl" == c.rel_delta.day);
    REQUIRE("ddd123878xc" == c.rel_delta.hour);
    REQUIRE("dddjsk8u123123" == c.rel_delta.minute);
    REQUIRE("ddd1u8923 6as79cyxc" == c.rel_delta.second);

    REQUIRE(c.rel_start.week_starts_monday);
    REQUIRE(c.rel_end.week_starts_monday);
    REQUIRE(c.rel_delta.week_starts_monday);

    REQUIRE("asbcyxcjhyxc" == c.format_header);
    REQUIRE("71823789xcjhk" == c.format_event);
    REQUIRE("1273xc67yxc781231" == c.format_footer);
    REQUIRE("knatata" == c.format_empty);
    REQUIRE("18927389xcyyxc" == c.format_date);
    REQUIRE("189237yxc" == c.format_time);
    REQUIRE("89127yxcnnn" == c.format_datetime);
}

TEST_CASE("set via parse_lines") {
    config c;

    string l1 = "";
    REQUIRE(c.parse_lines(l1));

    string l2 = "                               \n\
# comment                                       \n\
    # indented comment                          \n\
url abc\n\
        user     jaskdasd\n\
        pass     d   \n\
                  \n\
\n\
# the above line was empty, and the last line is too\n\
";
    REQUIRE(c.parse_lines(l2));
    REQUIRE("abc" == c.url);
    REQUIRE("jaskdasd" == c.auth_user);
    REQUIRE("d   " == c.auth_pass);

    string l3 = "\n\
url dddd\n\
unkown-key";
    REQUIRE(!c.parse_lines(l3));
    REQUIRE("dddd" == c.url);

    c.rel_delta.week_starts_monday = true;
    string l4 = "\n\
\n\
    \n\
# comment lalala\n\
week-start-sunday";
    REQUIRE(c.parse_lines(l4));
    REQUIRE(!c.rel_delta.week_starts_monday);
}

TEST_CASE("set via parse_file") {
    config c;

    // work on empty file
    REQUIRE(c.parse_file("/dev/null"));

    // note: the compiler will weep about tmpnam, but unfourtunately there is no other function to replace it
    string fname = std::tmpnam(nullptr);
    std::ofstream f(fname);

    REQUIRE(f.is_open());

    // proper file
    f << " # comment\n";
    f << "\n   \n\n";
    f << "   url https://example.com/";
    f.flush();

    REQUIRE(c.parse_file(fname));
    REQUIRE("https://example.com/" == c.url);

    SECTION("load file via argv") {
        // to create string pointers on stack
        char s00[] = "bin-name";
        char s01[] = "-c";
        std::shared_ptr<char[]> fname_copy(new char[1 + fname.size()]());
        strcpy(fname_copy.get(), fname.c_str());

        char* args[] = {s00, s01, fname_copy.get()};
        config c_from_args;
        REQUIRE("https://example.com/" != c_from_args.url);
        REQUIRE(c_from_args.parse_args(sizeof(args) / sizeof(args[0]), args));
        REQUIRE("https://example.com/" == c_from_args.url);
    }

    SECTION("load file via reference in lines") {
        // to create string pointers on stack
        config c_from_lines;
        REQUIRE("https://example.com/" != c_from_lines.url);
        REQUIRE(c_from_lines.parse_lines("#comment\n    cfg-file     " + fname));
        REQUIRE("https://example.com/" == c_from_lines.url);
    }

    // error(unkown key) in file
    f << "\n";
    f << "unkown-key absd";
    f.flush();

    REQUIRE(!c.parse_file(fname));
    REQUIRE("https://example.com/" == c.url);

    // fail on non-existent file
    f.close();
    std::remove(fname.c_str());

    REQUIRE(!c.parse_file(fname));
    REQUIRE(!c.parse_lines("cfg-file " + fname));
}

TEST_CASE("start/end calculation") {
    // simple test
    config c_simple;
    c_simple.rel_start.year = "2019";
    c_simple.rel_end.year = "2020";

    // use localtime, as cfg also specifies localtime only
    REQUIRE(119 == c_simple.period_start().get_tm_localtime().tm_year);
    REQUIRE(120 == c_simple.period_end().get_tm_localtime().tm_year);

    // set by epoch
    config c_epoch;
    c_epoch.start_timestamp = "1610503823";
    c_epoch.end_timestamp = "1610563823";

    REQUIRE(1610503823 == c_epoch.period_start().get_time_t_utc());
    REQUIRE(1610563823 == c_epoch.period_end().get_time_t_utc());

    // check duration
    config c_delta;
    // note: don't set hour to +1, as this will just put to next full hour, and not increase the timer by 3600 seconds
    c_delta.rel_delta.second = "+3600";
    REQUIRE(3600 == c_delta.period_end().get_time_t_utc() - c_delta.period_start().get_time_t_utc());

    // check calculation from delta
    config c_calc;
    // wednesday, don't care about timezones, as "next monday" will have the same date for each timezone
    c_calc.url = "i'm here to satisty the validity chech";
    c_calc.start_timestamp = "1610563823";
    c_calc.rel_delta.week = "+1";
    c_calc.parse_line("week-start-monday");
    REQUIRE(c_calc.is_valid());

    // calculation tested is based on localtime, so date will be the same
    REQUIRE("2021-01-18 00:00:00" == c_calc.period_end().get_string_localtime("%F %T"));

    // two ways to express "during next week"
    config c_floor;
    config c_ceil;

    // start at next week
    c_floor.rel_start.week = "+1";
    c_ceil.rel_start.week = "+1";

    // delta uses flooring, so this is the monday 00:00:00 of the week +1 *from the start*
    c_floor.rel_delta.week = "+1";
    // end uses ceiling, so this is the last second (sunday, 23:59:59) of the *current week*
    c_ceil.rel_end.week = "+1";

    REQUIRE(c_floor.period_start() == c_ceil.period_start());
    // c_floor has the first second of the day *after* the next week, c_ceil the last second of the next week
    REQUIRE(c_floor.period_end().get_time_t_utc() == 1 + c_ceil.period_end().get_time_t_utc());
}

{ pkgs ? import <nixpkgs> {},
  stdenv ? pkgs.stdenv,
}:
stdenv.mkDerivation {
  pname = "terminnanny";
  version = "0.2.1";

  src = ./.;

  buildInputs = with pkgs; [
    stdenv.cc cmake pkg-config
    libxmlxx
    libical
    spdlog
    neon
  ];
}


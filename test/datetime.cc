// Copyright (C) 2021 Rolf Pfeffertal
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#include <ctime>
#include <chrono>
#include <iomanip>
#include <sstream>

#include <terminnanny/datetime.h>
#include <terminnanny/relative_datetime.h>

using terminnanny::datetime;
using terminnanny::relative_datetime;

using std::chrono::system_clock;
using std::chrono::time_point;
using std::chrono::seconds;

using std::to_string;

using Catch::Matchers::Contains;

bool equal_tms(const tm& t1, const tm& t2) {
    return t1.tm_sec == t2.tm_sec &&
        t1.tm_min == t2.tm_min &&
        t1.tm_hour == t2.tm_hour &&
        t1.tm_mday == t2.tm_mday &&
        t1.tm_mon == t2.tm_mon &&
        t1.tm_year == t2.tm_year;
}

TEST_CASE("get UTC offset") {
    // calculate local TZ diff to UTC
    time_t seed_time_t = system_clock::to_time_t(system_clock::now());

    time_t time_t_local = std::mktime(std::localtime(&seed_time_t));
    time_t time_t_utc = std::mktime(std::gmtime(&seed_time_t));
    time_t deltat = time_t_local - time_t_utc;
    if (time_t_local == time_t_utc) {
        INFO("your local timezone is equal to UTC, maybe specify a different TZ for testing");
    }

    REQUIRE(deltat == datetime::get_utc_offset_sec());
}

TEST_CASE("construct, set & get from UTC") {
    // test that compilation
    datetime empty_dt1, empty_dt2;
    REQUIRE(empty_dt1.get_time_t_utc() == empty_dt2.get_time_t_utc());

    // test current time
    REQUIRE(datetime().get_time_t_utc() == system_clock::to_time_t(system_clock::now()));

    // this is the c++ way to construct time_t from timestamp. don't ask.
    time_t dt1_time_t = system_clock::to_time_t(time_point<system_clock>(seconds(1610472297)));
    tm dt1_tm;
    dt1_tm.tm_year = 121; // year 0 is 1900
    dt1_tm.tm_mon = 0; // month 0 is january
    dt1_tm.tm_mday = 12;
    dt1_tm.tm_hour = 17;
    dt1_tm.tm_min = 24;
    dt1_tm.tm_sec = 57;
    dt1_tm.tm_isdst = 0; // DST is off
    // mktime is localtime, so add offset to utc time
    REQUIRE(std::mktime(&dt1_tm) == dt1_time_t - datetime::get_utc_offset_sec());

    datetime dt1_from_time_t(dt1_time_t), dt1_from_tm(dt1_tm);

    REQUIRE(dt1_from_time_t.get_time_t_utc() == dt1_from_tm.get_time_t_utc());
    REQUIRE(dt1_from_time_t.get_time_t_utc() == dt1_time_t);

    REQUIRE(equal_tms(dt1_from_time_t.get_tm_utc(), dt1_from_tm.get_tm_utc()));
    // note: ignores wday, yday and dst
    REQUIRE(equal_tms(dt1_from_time_t.get_tm_utc(), dt1_tm));
}

TEST_CASE("set & get from & to localtime") {
    time_t seed_time_t = system_clock::to_time_t(system_clock::now());

    // calculate values on the fly, as we can't set the timezone
    tm tm_local = *std::localtime(&seed_time_t);
    tm tm_utc = *std::gmtime(&seed_time_t);

    if (0 == datetime::get_utc_offset_sec()) {
        INFO("your local timezone is equal to UTC, maybe specify a different TZ for testing");
    }
    
    datetime dt;

    dt.set_utc(seed_time_t);
    REQUIRE(dt.get_time_t_utc() == seed_time_t);
    REQUIRE(equal_tms(dt.get_tm_utc(), tm_utc));
    REQUIRE(equal_tms(dt.get_tm_localtime(), tm_local));

    dt.set_utc(tm_utc);
    REQUIRE(dt.get_time_t_utc() == seed_time_t);
    REQUIRE(equal_tms(dt.get_tm_utc(), tm_utc));
    REQUIRE(equal_tms(dt.get_tm_localtime(), tm_local));

    dt.set_localtime(tm_local);
    REQUIRE(dt.get_time_t_utc() == seed_time_t);
    REQUIRE(equal_tms(dt.get_tm_utc(), tm_utc));
    REQUIRE(equal_tms(dt.get_tm_localtime(), tm_local));
}

TEST_CASE("format to string (utc & localtime)") {
    // 2008-12-12 18:12:12 UTC
    time_t seed = system_clock::to_time_t(time_point<system_clock>(seconds(1229105532)));
    datetime dt(seed);

    // default format should contain all components (testing only some here)
    // note: test for 6, as in 6 pm because format might be locale dependent
    REQUIRE_THAT(dt.get_string_utc(), Contains("2008") && Contains("12") && (Contains("6") || Contains("18")));
    REQUIRE("2008-12-12" == dt.get_string_utc("%F"));
    REQUIRE("18:12:12" == dt.get_string_utc("%T"));

    // calculate local time
    tm tm_local = *std::localtime(&seed);

    std::stringstream ss_date, ss_time;
    ss_date << std::put_time(&tm_local, "%F");
    ss_time << std::put_time(&tm_local, "%T");

    REQUIRE(ss_date.str() == dt.get_string_localtime("%F"));
    REQUIRE(ss_time.str() == dt.get_string_localtime("%T"));
}

TEST_CASE("apply relative datetime in utc & localtime") {
    time_t seed = system_clock::to_time_t(time_point<system_clock>(seconds(1229105532)));
    relative_datetime noon, weekstart_monday, weekstart_sunday, neutral, pi_day, first_of_month, end_of_the_world, minute_start, two_hours_ago, two_hours_ago_oclock, yesterday, in_10_min, in_90_min, in_90_min_2, in_7_days, in_7_days_keep_min, tuesday_last_week, february, next_year;

    noon.hour = "12";

    weekstart_monday.week_starts_monday = true;
    weekstart_monday.week = "-0";
    weekstart_sunday.week_starts_monday = false;
    weekstart_sunday.week = "-0";

    pi_day.month = "3";
    pi_day.day = "14";

    first_of_month.day = "1";

    end_of_the_world.year = "2012";
    end_of_the_world.month = "12";
    end_of_the_world.day = "21";

    minute_start.minute = "-0";

    two_hours_ago.hour = "-2";
    two_hours_ago.minute = "+0";
    two_hours_ago.second = "-0";

    two_hours_ago_oclock.hour = "-2";

    yesterday.day = "-1";

    in_10_min.minute = "+10";
    in_90_min.minute = "+90";

    in_90_min_2.hour = "-2";
    in_90_min_2.minute = "+120";
    in_90_min_2.second = "+5400";

    in_7_days.day = "+7";
    in_7_days_keep_min.day = "+7";
    in_7_days_keep_min.minute = "+0";

    tuesday_last_week.week = "-1";
    tuesday_last_week.week_starts_monday = true;
    tuesday_last_week.day = "+1";

    february.month = "2";

    next_year.year = "+1";

    time_t deltat = datetime::get_utc_offset_sec();
    if (0 == deltat) {
        INFO("your local timezone is equal to UTC, maybe specify a different TZ for testing");
    }

    datetime dt;
    time_t utc;

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(neutral);
    REQUIRE("2008-12-12 18:12:12" == dt.get_string_utc("%F %T"));
    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(neutral, true);
    REQUIRE("2008-12-12 18:12:12" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(neutral);
    // no difference, as the time is not changed at all
    REQUIRE((utc - dt.get_time_t_utc()) == 0);
    time_t buff = dt.get_time_t_utc();
    dt.apply_relative_datetime_localtime(neutral, true);
    REQUIRE(buff == dt.get_time_t_utc());
    
    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(noon);
    REQUIRE("2008-12-12 12:00:00" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(noon);
    // note: even though a UTC timestamp is requested, it is set using information from localtime, so it should be apart from the earlier timestamp
    REQUIRE((utc - dt.get_time_t_utc()) == deltat);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(noon, true);
    REQUIRE("2008-12-12 12:59:59" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(noon, true);
    REQUIRE((utc - dt.get_time_t_utc()) == deltat);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(weekstart_monday);
    REQUIRE("2008-12-08 00:00:00" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(weekstart_monday);
    REQUIRE((utc - dt.get_time_t_utc()) == deltat);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(weekstart_monday, true);
    REQUIRE("2008-12-14 23:59:59" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(weekstart_monday, true);
    REQUIRE((utc - dt.get_time_t_utc()) == deltat);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(weekstart_sunday);
    REQUIRE("2008-12-07 00:00:00" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(weekstart_sunday);
    REQUIRE((utc - dt.get_time_t_utc()) == deltat);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(weekstart_sunday, true);
    REQUIRE("2008-12-13 23:59:59" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(weekstart_sunday, true);
    REQUIRE((utc - dt.get_time_t_utc()) == deltat);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(pi_day);
    REQUIRE("2008-03-14 00:00:00" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(pi_day);
    REQUIRE((utc - dt.get_time_t_utc()) == deltat);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(pi_day, true);
    REQUIRE("2008-03-14 23:59:59" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(pi_day, true);
    REQUIRE((utc - dt.get_time_t_utc()) == deltat);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(first_of_month);
    REQUIRE("2008-12-01 00:00:00" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(first_of_month);
    REQUIRE((utc - dt.get_time_t_utc()) == deltat);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(first_of_month, true);
    REQUIRE("2008-12-01 23:59:59" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(first_of_month, true);
    REQUIRE((utc - dt.get_time_t_utc()) == deltat);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(end_of_the_world);
    REQUIRE("2012-12-21 00:00:00" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(end_of_the_world);
    REQUIRE((utc - dt.get_time_t_utc()) == deltat);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(end_of_the_world, true);
    REQUIRE("2012-12-21 23:59:59" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(end_of_the_world, true);
    REQUIRE((utc - dt.get_time_t_utc()) == deltat);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(minute_start);
    REQUIRE("2008-12-12 18:12:00" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(minute_start);
    // only seconds modified -> no differene exected
    REQUIRE((utc - dt.get_time_t_utc()) == 0);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(minute_start, true);
    REQUIRE("2008-12-12 18:12:59" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(minute_start, true);
    REQUIRE((utc - dt.get_time_t_utc()) == 0);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(two_hours_ago);
    REQUIRE("2008-12-12 16:12:12" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(two_hours_ago);
    // purely relative -> no difference expected
    REQUIRE((utc - dt.get_time_t_utc()) == 0);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(two_hours_ago, true);
    REQUIRE("2008-12-12 16:12:12" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(two_hours_ago, true);
    REQUIRE((utc - dt.get_time_t_utc()) == 0);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(two_hours_ago_oclock);
    REQUIRE("2008-12-12 16:00:00" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(two_hours_ago_oclock);
    // purely relative -> no difference expected
    REQUIRE((utc - dt.get_time_t_utc()) == 0);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(two_hours_ago_oclock, true);
    REQUIRE("2008-12-12 16:59:59" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(two_hours_ago_oclock, true);
    REQUIRE((utc - dt.get_time_t_utc()) == 0);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(yesterday);
    REQUIRE("2008-12-11 00:00:00" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(yesterday);
    REQUIRE((utc - dt.get_time_t_utc()) == deltat);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(yesterday, true);
    REQUIRE("2008-12-11 23:59:59" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(yesterday, true);
    REQUIRE((utc - dt.get_time_t_utc()) == deltat);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(in_10_min);
    REQUIRE("2008-12-12 18:22:00" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(in_10_min);
    // only relative minutes changed -> no change expected
    REQUIRE((utc - dt.get_time_t_utc()) == 0);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(in_10_min, true);
    REQUIRE("2008-12-12 18:22:59" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(in_10_min, true);
    // only relative minutes changed -> no change expected
    REQUIRE((utc - dt.get_time_t_utc()) == 0);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(in_90_min);
    REQUIRE("2008-12-12 19:42:00" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(in_90_min);
    // only relative minutes changed -> no change expected
    REQUIRE((utc - dt.get_time_t_utc()) == 0);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(in_90_min, true);
    REQUIRE("2008-12-12 19:42:59" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(in_90_min, true);
    REQUIRE((utc - dt.get_time_t_utc()) == 0);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(in_90_min_2);
    REQUIRE("2008-12-12 19:42:12" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(in_90_min_2);
    // purly relative -> no change expected
    REQUIRE((utc - dt.get_time_t_utc()) == 0);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(in_90_min_2, true);
    REQUIRE("2008-12-12 19:42:12" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(in_90_min_2, true);
    REQUIRE((utc - dt.get_time_t_utc()) == 0);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(in_7_days);
    REQUIRE("2008-12-19 00:00:00" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(in_7_days);
    REQUIRE((utc - dt.get_time_t_utc()) == deltat);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(in_7_days, true);
    REQUIRE("2008-12-19 23:59:59" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(in_7_days, true);
    REQUIRE((utc - dt.get_time_t_utc()) == deltat);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(in_7_days_keep_min);
    REQUIRE("2008-12-19 00:12:00" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(in_7_days_keep_min);
    REQUIRE((utc - dt.get_time_t_utc()) == deltat);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(in_7_days_keep_min, true);
    REQUIRE("2008-12-19 00:12:59" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(in_7_days_keep_min, true);
    REQUIRE((utc - dt.get_time_t_utc()) == deltat);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(tuesday_last_week);
    REQUIRE("2008-12-02 00:00:00" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(tuesday_last_week);
    REQUIRE((utc - dt.get_time_t_utc()) == deltat);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(tuesday_last_week, true);
    REQUIRE("2008-12-02 23:59:59" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(tuesday_last_week, true);
    REQUIRE((utc - dt.get_time_t_utc()) == deltat);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(february);
    REQUIRE("2008-02-01 00:00:00" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(february);
    REQUIRE((utc - dt.get_time_t_utc()) == deltat);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(february, true);
    REQUIRE("2008-02-29 23:59:59" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(february, true);
    REQUIRE((utc - dt.get_time_t_utc()) == deltat);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(next_year);
    REQUIRE("2009-01-01 00:00:00" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(next_year);
    REQUIRE((utc - dt.get_time_t_utc()) == deltat);

    dt.set_utc(seed);
    dt.apply_relative_datetime_utc(next_year, true);
    REQUIRE("2009-12-31 23:59:59" == dt.get_string_utc("%F %T"));
    utc = dt.get_time_t_utc();
    dt.set_utc(seed);
    dt.apply_relative_datetime_localtime(next_year, true);
    REQUIRE((utc - dt.get_time_t_utc()) == deltat);
}

TEST_CASE("ordering") {
    datetime dt_1a(123);
    datetime dt_1b(123);
    datetime dt_2a(124);
    datetime dt_3a(125);

    // op ==
    REQUIRE( (dt_1a == dt_1a));
    REQUIRE( (dt_1a == dt_1b));
    REQUIRE(!(dt_1a == dt_2a));
    REQUIRE(!(dt_1a == dt_3a));

    REQUIRE( (dt_1b == dt_1a));
    REQUIRE( (dt_1b == dt_1b));
    REQUIRE(!(dt_1b == dt_2a));
    REQUIRE(!(dt_1b == dt_3a));

    REQUIRE(!(dt_2a == dt_1a));
    REQUIRE(!(dt_2a == dt_1b));
    REQUIRE( (dt_2a == dt_2a));
    REQUIRE(!(dt_2a == dt_3a));

    REQUIRE(!(dt_3a == dt_1a));
    REQUIRE(!(dt_3a == dt_1b));
    REQUIRE(!(dt_3a == dt_2a));
    REQUIRE( (dt_3a == dt_3a));

    // op !=
    REQUIRE(!(dt_1a != dt_1a));
    REQUIRE(!(dt_1a != dt_1b));
    REQUIRE( (dt_1a != dt_2a));
    REQUIRE( (dt_1a != dt_3a));

    REQUIRE(!(dt_1b != dt_1a));
    REQUIRE(!(dt_1b != dt_1b));
    REQUIRE( (dt_1b != dt_2a));
    REQUIRE( (dt_1b != dt_3a));

    REQUIRE( (dt_2a != dt_1a));
    REQUIRE( (dt_2a != dt_1b));
    REQUIRE(!(dt_2a != dt_2a));
    REQUIRE( (dt_2a != dt_3a));

    REQUIRE( (dt_3a != dt_1a));
    REQUIRE( (dt_3a != dt_1b));
    REQUIRE( (dt_3a != dt_2a));
    REQUIRE(!(dt_3a != dt_3a));

    // op <
    REQUIRE(!(dt_1a < dt_1a));
    REQUIRE(!(dt_1a < dt_1b));
    REQUIRE( (dt_1a < dt_2a));
    REQUIRE( (dt_1a < dt_3a));

    REQUIRE(!(dt_1b < dt_1a));
    REQUIRE(!(dt_1b < dt_1b));
    REQUIRE( (dt_1b < dt_2a));
    REQUIRE( (dt_1b < dt_3a));

    REQUIRE(!(dt_2a < dt_1a));
    REQUIRE(!(dt_2a < dt_1b));
    REQUIRE(!(dt_2a < dt_2a));
    REQUIRE( (dt_2a < dt_3a));

    REQUIRE(!(dt_3a < dt_1a));
    REQUIRE(!(dt_3a < dt_1b));
    REQUIRE(!(dt_3a < dt_2a));
    REQUIRE(!(dt_3a < dt_3a));

    // op >
    REQUIRE(!(dt_1a > dt_1a));
    REQUIRE(!(dt_1a > dt_1b));
    REQUIRE(!(dt_1a > dt_2a));
    REQUIRE(!(dt_1a > dt_3a));

    REQUIRE(!(dt_1b > dt_1a));
    REQUIRE(!(dt_1b > dt_1b));
    REQUIRE(!(dt_1b > dt_2a));
    REQUIRE(!(dt_1b > dt_3a));

    REQUIRE( (dt_2a > dt_1a));
    REQUIRE( (dt_2a > dt_1b));
    REQUIRE(!(dt_2a > dt_2a));
    REQUIRE(!(dt_2a > dt_3a));

    REQUIRE( (dt_3a > dt_1a));
    REQUIRE( (dt_3a > dt_1b));
    REQUIRE( (dt_3a > dt_2a));
    REQUIRE(!(dt_3a > dt_3a));

    // op <=
    REQUIRE( (dt_1a <= dt_1a));
    REQUIRE( (dt_1a <= dt_1b));
    REQUIRE( (dt_1a <= dt_2a));
    REQUIRE( (dt_1a <= dt_3a));

    REQUIRE( (dt_1b <= dt_1a));
    REQUIRE( (dt_1b <= dt_1b));
    REQUIRE( (dt_1b <= dt_2a));
    REQUIRE( (dt_1b <= dt_3a));

    REQUIRE(!(dt_2a <= dt_1a));
    REQUIRE(!(dt_2a <= dt_1b));
    REQUIRE( (dt_2a <= dt_2a));
    REQUIRE( (dt_2a <= dt_3a));

    REQUIRE(!(dt_3a <= dt_1a));
    REQUIRE(!(dt_3a <= dt_1b));
    REQUIRE(!(dt_3a <= dt_2a));
    REQUIRE( (dt_3a <= dt_3a));

    // op >=
    REQUIRE( (dt_1a >= dt_1a));
    REQUIRE( (dt_1a >= dt_1b));
    REQUIRE(!(dt_1a >= dt_2a));
    REQUIRE(!(dt_1a >= dt_3a));

    REQUIRE( (dt_1b >= dt_1a));
    REQUIRE( (dt_1b >= dt_1b));
    REQUIRE(!(dt_1b >= dt_2a));
    REQUIRE(!(dt_1b >= dt_3a));

    REQUIRE( (dt_2a >= dt_1a));
    REQUIRE( (dt_2a >= dt_1b));
    REQUIRE( (dt_2a >= dt_2a));
    REQUIRE(!(dt_2a >= dt_3a));

    REQUIRE( (dt_3a >= dt_1a));
    REQUIRE( (dt_3a >= dt_1b));
    REQUIRE( (dt_3a >= dt_2a));
    REQUIRE( (dt_3a >= dt_3a));
}

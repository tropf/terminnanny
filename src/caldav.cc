// Copyright (C) 2021 Rolf Pfeffertal
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#include <terminnanny/caldav.h>

#include <string>
#include <cstring>
#include <memory>
#include <utility>

#include <spdlog/spdlog.h>

#include <neon/ne_props.h>
#include <neon/ne_session.h>
#include <neon/ne_request.h>
#include <neon/ne_auth.h>
#include <neon/ne_uri.h>

#include <terminnanny/datetime.h>

using std::string;
using terminnanny::datetime;

const string terminnanny::caldav::ical_date_format = "%Y%m%dT%H%M%SZ";

terminnanny::caldav::caldav(const string& _uri_str,
                            const datetime& _from,
                            const datetime& _to,
                            const string& _auth_user,
                            const string& _auth_pass) :
    from(_from), to(_to), auth_user(_auth_user), auth_pass(_auth_pass), uri({nullptr, nullptr, nullptr, 0, nullptr, nullptr, nullptr}) {
    // ignore result, will be logged anyways
    parse_uri(_uri_str);
}

terminnanny::caldav::~caldav() {
    // doesn't free actual object but its subcomponents
    ne_uri_free(&uri);

    // this is somewhat dirty, please look away children
    memset((void*) auth_user.data(), '*', auth_user.size());
    memset((void*) auth_pass.data(), '*', auth_pass.size());
}

void terminnanny::caldav::set_auth_user(const string& s) {
    auth_user = s;
}

void terminnanny::caldav::set_auth_pass(const string& s) {
    auth_pass = s;
}

ne_uri terminnanny::caldav::get_uri() const {
    return uri;
}

bool terminnanny::caldav::parse_uri(const string& s) {
    if (s.empty()) {
        spdlog::trace("passed empty url, abort parsing");
        return false;
    }

    if (0 != ne_uri_parse(s.c_str(), &uri)) {
        spdlog::error("could not parse URI {}", s);
        return false;
    }

    if (0 == uri.port) {
        if (nullptr != uri.scheme) {
            spdlog::debug("guessing port from scheme...");
            uri.port = ne_uri_defaultport(uri.scheme);
        } else {
            spdlog::warn("port not provided, and could not guess from scheme");
            return false;
        }
    }
    spdlog::trace("using port {}", uri.port);

    return true;
}

void terminnanny::caldav::set_period(const datetime& _from, const datetime& _to) {
    set_period_start(_from);
    set_period_end(_to);
}

void terminnanny::caldav::set_period_start(const datetime& dt) {
    from = dt;
}

void terminnanny::caldav::set_period_end(const datetime& dt) {
    to = dt;
}

datetime terminnanny::caldav::get_period_start() const {
    return from;
}

datetime terminnanny::caldav::get_period_end() const {
    return to;
}

string terminnanny::caldav::build_events_request_body() const {
    return \
        "<?xml version=\"1.0\" encoding=\"utf-8\" ?>"
        "<C:calendar-query xmlns:C=\"urn:ietf:params:xml:ns:caldav\">"
        "  <D:prop xmlns:D=\"DAV:\">"
        "    <D:getetag/>"
        "    <C:calendar-data/>"
        "  </D:prop>"
        "  <C:filter>"
        "    <C:comp-filter name=\"VCALENDAR\">"
        "      <C:comp-filter name=\"VEVENT\">"
        "        <C:time-range start=\"" + from.get_string_utc(ical_date_format) + "\" end=\"" + to.get_string_utc(ical_date_format) + "\"/>"
        "      </C:comp-filter>"
        "    </C:comp-filter>"
        "  </C:filter>"
        "</C:calendar-query>";
}

static int auth_cb(void* userdata, const char* realm, int tries, char* user, char* pass) {
    std::pair<string, string>* credentials_ptr = (std::pair<string, string>*) userdata;
    strcpy(user, credentials_ptr->first.c_str());
    strcpy(pass, credentials_ptr->second.c_str());
    spdlog::debug("supplied credentials for realm '{}': {} with pw {}", realm, user, string(strlen(pass), '*'));
    return tries;
}

bool terminnanny::caldav::request_events() {
    spdlog::trace("initializing session", uri.port);
    ne_session* session = ne_session_create(uri.scheme, uri.host, uri.port);
    ne_ssl_trust_default_ca(session);

    // TODO replace with a safer credential store
    std::pair<string, string> credentials;

    if (!auth_pass.empty() || !auth_user.empty()) {
        spdlog::debug("credentials supplied, attaching auth handler");

        credentials.first = auth_user;
        credentials.second = auth_pass;
        
        ne_set_server_auth(session, auth_cb, &credentials);
    } else {
        spdlog::debug("no credentials supplied, not attaching auth handler");
    }

    spdlog::trace("preparing preflight request...");
    // preflight request to init auth
    ne_request* preflight_req;
    preflight_req = ne_request_create(session, "OPTIONS", uri.path);
    spdlog::debug("sending preflight request...");
    auto preflight_resp = ne_request_dispatch(preflight_req);
    if (NE_ERROR == preflight_resp) {
        spdlog::error("preflight failed with: {}", ne_get_error(session));
        return false;
    } else if (NE_CONNECT == preflight_resp) {
        spdlog::error("could not establish connection");
        return false;
    } else if (NE_TIMEOUT == preflight_resp) {
        spdlog::error("preflight request reached timeout");
        return false;
    } else if (NE_AUTH == preflight_resp) {
        spdlog::warn("authentication error during preflight");
    } else if (NE_OK != preflight_resp) {
        spdlog::error("unspecified error during preflight request: {}", ne_get_error(session));
        return false;
    }
    ne_request_destroy(preflight_req);
    spdlog::debug("preflight request success, sending actual request");

    spdlog::trace("assembling actual request...");
    ne_request* req;
    req = ne_request_create(session, "REPORT", uri.path);
    string req_body_str = build_events_request_body();
    ne_set_request_body_buffer(req, req_body_str.c_str(), req_body_str.size());
    ne_add_request_header(req, "Depth", "1");

    int resp_status = ne_begin_request(req);
    if (NE_OK != resp_status) {
        spdlog::error("could not send request (status {}): {}", resp_status, ne_get_error(session));
        return false;
    }
    spdlog::info("request response: {}", ne_get_error(session));

    response_body = "";
    size_t bytes_read = 1;
    while(bytes_read > 0) {
        char buff[1024] = {0};
        bytes_read = ne_read_response_block(req, buff, sizeof(buff) - 1);
        response_body += string(buff, bytes_read);
        spdlog::debug("read {} bytes", bytes_read);
        spdlog::trace("read {} bytes: {}", bytes_read, string(buff, bytes_read));
    }

    spdlog::debug("request finished, cleaning up...");
    ne_end_request(req);
    ne_request_destroy(req);
    ne_session_destroy(session);

    return true;
}

string terminnanny::caldav::get_request_result() const {
    return response_body;
}

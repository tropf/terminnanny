# terminnanny
generate calendar overviews

This is a simple tool that consumes a (caldav) calendar,
a somewhat human-readable start and end date,
some formatstring (optional) and from that prints out a human-readable calendar summary.

Its intended usecase are mainly community calendars to feed the generated summaries into chatbots and alike.

It's documentation comes mainly in form of a manual page.

## Installing and Building
**Binary Release**  
[Debian](https://software.opensuse.org//download.html?project=home%3Atropf%3Aterminnanny&package=terminnanny)
| [Ubuntu](https://software.opensuse.org//download.html?project=home%3Atropf%3Aterminnanny&package=terminnanny)

```
cd terminnanny
mkdir build && cd build
cmake ..
make -j $(( 2 * $(nproc) ))
sudo make install
```

## Examples
For details please have a look at the provided manpage.
The date format was designed for ease of use, but is still complicated.
Also refer to `terminnanny --help` for a list of all options.

```
$ terminnanny --url 'https://example.com/caldav/mycal/' --user alice --pass hackme --from-week +1 --delta-week +1
2021-01-19 18:30:00 @ : Nerdy Tuesday
2021-01-20 17:00:00 @ : Baking together
2021-01-22 11:11:00 @ Location: Testtitle
2021-01-24 11:11:00 @ Location: Testtitle
```

```
$ terminnanny --url 'https://example.com/caldav/mycal/' --user alice --pass hackme --from-month 2 --to-month 3
2021-02-19 18:30:00 @ Clubhouse: Cooking Session
2021-02-20 17:00:00 @ Clubhouse: Beer Tasting
2021-03-12 14:00:00 @ Garden: Workshop Gardening
2021-03-24 19:00:00 @ Garden: Workshop Gardening (rerun)
```

with config file `calendar_cfg`:

```
# location & credentials
url https://example.com/caldav/othercal
user alice
pass hackme

# format for dates (for a german audience)
format-date %d.%m
format-time %H:%M Uhr
format-datetime %A, %d. %B um %H:%M Uhr

# format actual output
format-header Termine von %PERIOD_START_DATE% bis %PERIOD_END_DATE%
format-event %DATETIME% @ %LOCATION%\n**%TITLE%**\n%DESCRIPTION%\n
```

can then be included:

```
# specifying any component with "+0" or "-0" it is set to the current
# this examples shows everything from the current month
$ terminnanny -c calendar_cfg --from-month=-0 --to-month=-0
Termine von 01.01 bis 31.01
Freitag, 01. Januar um 16:00 Uhr @ Meeting Room 1
**Team Meeting**
Everyone gets together to talk about the most important issues.

Dienstag, 05. Januar um 18:30 Uhr @ Basement
**Gaming Session**
We play our favorite games together.

Dienstag, 12. Januar um 18:30 Uhr @ Basement
**Gaming Session**
We play our favorite games together.

Freitag, 15. Januar um 16:00 Uhr @ Meeting Room 1
**Team Meeting**
Everyone gets together to talk about the most important issues.

Dienstag, 19. Januar um 18:30 Uhr @ Basement
**Gaming Session**
We play our favorite games together.

Dienstag, 26. Januar um 18:30 Uhr @ Basement
**Gaming Session**
We play our favorite games together.

Freitag, 29. Januar um 16:00 Uhr @ Meeting Room 1
**Team Meeting**
Everyone gets together to talk about the most important issues.
```

## Misc
**Is this stable? Can I use this? Roadmap?**  
No, and probably.
This piece of code exists to replace an existing setup of several shell scripts,
and is a typical case of "works for me".
That means I will probably maintain it, but I don't plan to add any features.
The code itself is actually tested ok-ish,
but all sections involving library calls are not tested automatically,
because that requires some more work.
Also I have not checked what happens on malicious server responses.

**Feedback**  
I'd be happy to hear from you!
Just shoot me a message: `terminnanny at tropf dot io`.

**Contributing**  
Just do it

**Cite**  
I don't know why anyone would cite this,
but I have missed this section in quite a lot of projects,
and would like to see it become standard.
So here you go (please update the dates to the latest commit):

``` bibtex
@Misc{terminnanny,
  author       = {Rolf Pfeffertal},
  title        = {terminnanny},
  howpublished = {\url{https://codeberg.org/tropf/terminnanny}},
  month        = {CHANGEME},
  year         = {CHANGEME},
  abstract     = {generate calendar overviews},
  url          = {https://codeberg.org/tropf/terminnanny},
}
```

**Libraries**  
[argp](https://www.gnu.org/software/libc/manual/html_node/Argp.html)
[libxml++](https://developer.gnome.org/libxml++/)
[libical](https://github.com/libical/libical/)
[spdlog](https://github.com/gabime/spdlog/)
[neon](https://github.com/notroj/neon/)
[catch](https://github.com/catchorg/Catch2/)

Thank you for these awesome libs, even if they caused me some headaches while including.
And also thanks to the the packagers that made my development very smooth.

**License**  
This project is available under the [GNU GPLv3+](COPYING).

**Online**  
Find this project on [Codeberg](https://codeberg.org/tropf/terminnanny).

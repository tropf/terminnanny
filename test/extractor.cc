// Copyright (C) 2021 Rolf Pfeffertal
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#include <string>
#include <algorithm>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <libxml++/libxml++.h>
#pragma GCC diagnostic pop

#include <terminnanny/extractor.h>

using std::string;
using std::find;
using terminnanny::extractor;

TEST_CASE("empty doc") {
    extractor x;
    REQUIRE_THROWS_AS(x.parse_string(""), xmlpp::exception);
}

TEST_CASE("errornous doc") {
    extractor x;
    REQUIRE_THROWS_AS(x.parse_string("<html></html>too much content"), xmlpp::exception);
}

TEST_CASE("normal doc") {
    string doc = "<d:multistatus xmlns:d=\"DAV:\" xmlns:s=\"http://sabredav.org/ns\" xmlns:cal=\"urn:ietf:params:xml:ns:caldav\" xmlns:cs=\"http://calendarserver.org/ns/\" xmlns:oc=\"http://owncloud.org/ns\" xmlns:nc=\"http://nextcloud.org/ns\"><d:response><d:href>/url/path.ic</d:href><d:propstat><d:prop><d:getetag>&quot;81237189sxfcyxcyx&quot;</d:getetag><cal:calendar-data>DATA1</cal:calendar-data><cal:calendar-data>secretxmlstuff&lt;</cal:calendar-data></d:prop><d:status>HTTP/1.1 200 OK</d:status></d:propstat></d:response></d:multistatus>";

    extractor x;
    auto data = x.get_icals(doc);
    REQUIRE(find(data.begin(), data.end(), "secretxmlstuff<") != data.end());
    REQUIRE(find(data.begin(), data.end(), "DATA1") != data.end());
    REQUIRE(find(data.begin(), data.end(), "DATA2") == data.end());
}

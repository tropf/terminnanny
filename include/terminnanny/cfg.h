// Copyright (C) 2021 Rolf Pfeffertal
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef __TERMINNANNY_CFG_H_INClUDED__
#define __TERMINNANNY_CFG_H_INClUDED__

#include <string>
#include <argp.h>

#include <terminnanny/datetime.h>
#include <terminnanny/relative_datetime.h>

using std::string;

extern const char* argp_program_version;
extern const char* argp_program_bug_address;

namespace terminnanny {
    class config {
    private:
        const datetime dt_init;
    public:
        static const struct argp_option options[];
        string url = "";
        string auth_user = "";
        string auth_pass = "";
        string format_header = "";
        string format_event = "%DATETIME% @ %LOCATION%: %TITLE%";
        string format_footer = "";
        string format_empty = "";
        string format_date = "%F";
        string format_time = "%T";
        string format_datetime = "%F %T";

        relative_datetime rel_start, rel_end, rel_delta;
        string start_timestamp = "";
        string end_timestamp = "";

        enum argp_options : int {
            url_option = 'u',
            verbosity = 'v',
            cfg_file = 'c',

            // initalize to some key that has no conflict with ascii
            pass = 1337,
            user,
            from_year, from_month, from_week, from_day, from_hour, from_minute, from_second,
            to_year, to_month, to_week, to_day, to_hour, to_minute, to_second,
            delta_year, delta_month, delta_week, delta_day, delta_hour, delta_minute, delta_second,
            from_epoch, to_epoch,
            week_start_monday,
            week_start_sunday,

            format_header_opt,
            format_event_opt,
            format_footer_opt,
            format_empty_opt,
            format_datetime_opt,
            format_date_opt,
            format_time_opt,
        };

        // get examined period start
        datetime period_start() const;
        // get examined period end
        datetime period_end() const;
        
        /// parses a single option, invoked by argp
        int parse_single_opt(int, const char*, struct argp_state*);

        /// load file and parse its content, return false on failure
        bool parse_file(const string& path);
        /// split lines & parse them as configuration, return false on failure
        bool parse_lines(const string&);
        /// parse line as cfg, must be correct format, return false on failure
        bool parse_line(const string&);

        /// "" on no error
        string get_validation_error() const;

        /// true iff config is valid
        bool is_valid() const;

        /// parses args, returns false if parsing error occured, config MIGHT BE INVALID
        bool parse_args(int argc, char** argv, unsigned argp_flags = 0);

        /// loads cfg from cmdline args, exits program if appropriate, config MIGHT BE INVALID
        void try_parse_args(int argc, char** argv, unsigned argp_flags = 0);
    };

    extern config cfg;
}

#endif // __TERMINNANNY_CFG_H_INClUDED__

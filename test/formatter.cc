// Copyright (C) 2021 Rolf Pfeffertal
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#include <terminnanny/formatter.h>

#include <string>
#include <map>

#include <terminnanny/event.h>
#include <terminnanny/cfg.h>

using std::string;
using std::map;
using terminnanny::datetime;
using terminnanny::event;
using terminnanny::formatter;
using terminnanny::config;

TEST_CASE("replacement") {
    SECTION("simple example") {
        formatter f;
        f.replacements["$name"] = "Peter";
        f.replacements["%TITLE%"] = "awesome event";
        string pattern = "Hey $name, do you want to come to %TITLE%?";
        const string expect = "Hey Peter, do you want to come to awesome event?";

        REQUIRE(f.get_replaced(pattern) == expect);
        REQUIRE(pattern != expect);
        f.replace(pattern);
        REQUIRE(pattern == expect);

        REQUIRE("PeterPeter" == f.get_replaced("$name$name"));
    }

    SECTION("empty map") {
        formatter f;
        f.replacements.clear();
        REQUIRE("$name" == f.get_replaced("$name"));
        REQUIRE("" == f.get_replaced(""));
        REQUIRE("\\\\" == f.get_replaced("\\\\"));
    }

    SECTION("circular replacements") {
        formatter f;
        f.replacements["this"] = "that";
        f.replacements["that"] = "this";
        string pattern = "thisthat";
        const string expect = "thatthis";

        REQUIRE(f.get_replaced(pattern) == expect);
        REQUIRE(pattern != expect);
        f.replace(pattern);
        REQUIRE(pattern == expect);
    }

    SECTION("deletion") {
        formatter f;
        f.replacements["gone"] = "";
        string pattern = "this is: gonegonegone";
        const string expect = "this is: ";
        REQUIRE(f.get_replaced(pattern) == expect);
        REQUIRE(pattern != expect);
        f.replace(pattern);
        REQUIRE(pattern == expect);
    }

    SECTION("empty string") {
        formatter f;
        f.replacements[""] = "xx";
        const string pattern = "This sentence no verb.";
        REQUIRE(pattern == f.get_replaced(pattern));
    }

    SECTION("regex control characters") {
        formatter f;
        f.replacements["$"] = "ENDING";
        f.replacements["^"] = "BEGINNING";
        const string pattern = "This sentence no verb.";
        REQUIRE(pattern == f.get_replaced(pattern));
    }
}

TEST_CASE("default replacements") {
    formatter f;
    // \n -> actual newline
    REQUIRE("\n" == f.get_replaced("\\n"));
    // \\ -> \        (i am a line trailer that the comment does not stretch across 2 lines due to a trailing \)
    REQUIRE("\\" == f.get_replaced("\\\\"));
    REQUIRE("\\content\n" == f.get_replaced("\\\\content\n"));

    f.replacements.clear();
    REQUIRE("\\n" == f.get_replaced("\\n"));
    REQUIRE("\\\\" == f.get_replaced("\\\\"));
    REQUIRE("\\\\content\n" == f.get_replaced("\\\\content\n"));
}

TEST_CASE("strip") {
    REQUIRE("" == formatter::get_stripped(""));
    REQUIRE("" == formatter::get_stripped("\n\n\t   "));
    REQUIRE("hello" == formatter::get_stripped("\n\n\t   hello\n"));
    REQUIRE("text" == formatter::get_stripped("text"));
    REQUIRE("inner\nnewline" == formatter::get_stripped("inner\nnewline"));
    REQUIRE("only trailing stuff" == formatter::get_stripped("only trailing stuff   "));
    REQUIRE("only trailing stuff" == formatter::get_stripped("only trailing stuff   \n"));
    REQUIRE("newlines" == formatter::get_stripped("\n\n  \t\n  newlines\n\n"));
    REQUIRE("single\ntrailing\nnewline" == formatter::get_stripped("single\ntrailing\nnewline\n"));
}

TEST_CASE("loading data from events") {
    SECTION("simple example") {
        event e;
        e.start = datetime(1610702147);
        // text fields will be stripped
        e.title = "awesome event   ";
        e.location = "\toutside  ";
        e.description = "sentence \n stuff\n";
        
        formatter f;
        f.format_date = "%Y";
        f.format_datetime = "%Y";
        f.fill_from_event(e);

        REQUIRE("awesome event" == f.replacements["%TITLE%"]);
        REQUIRE("outside" == f.replacements["%LOCATION%"]);
        REQUIRE("sentence \n stuff" == f.replacements["%DESCRIPTION%"]);
        REQUIRE("2021" == f.replacements["%DATE%"]);
        REQUIRE("2021" == f.replacements["%DATETIME%"]);
    }

    SECTION("empty strings") {
        event e;
        formatter f;
        f.fill_from_event(e);

        // note: .at() throws if key has not been set before (while [] will initialize the key if it doesn't exist)
        REQUIRE_NOTHROW("" == f.replacements.at("%TITLE%"));
        REQUIRE_NOTHROW("" == f.replacements.at("%LOCATION%"));
        REQUIRE_NOTHROW("" == f.replacements.at("%DESCRIPTION%"));

        // just check that key exists
        REQUIRE_NOTHROW(f.replacements.at("%DATE%"));
        REQUIRE_NOTHROW(f.replacements.at("%DATETIME%"));
        REQUIRE_NOTHROW(f.replacements.at("%TIME%"));
    }

    SECTION("formatstrings are respected") {
        event e;
        e.start = datetime(1610702147);

        formatter f;
        f.format_date = "date";
        f.format_datetime = "datetime";
        f.format_time = "time";

        f.fill_from_event(e);
        REQUIRE("date" == f.replacements["%DATE%"]);
        REQUIRE("datetime" == f.replacements["%DATETIME%"]);
        REQUIRE("time" == f.replacements["%TIME%"]);

        // pass arbitrary formats, even if they don't make sense
        f.format_date = "%m%m%Y%Y";
        f.format_datetime = "%m%%";
        f.format_time = "%Y%Y%Y%%";

        f.fill_from_event(e);
        REQUIRE("010120212021" == f.replacements["%DATE%"]);
        REQUIRE("01%" == f.replacements["%DATETIME%"]);
        REQUIRE("202120212021%" == f.replacements["%TIME%"]);
    }
}

TEST_CASE("loading data from config") {
    SECTION("simple example") {
        config c;
        c.rel_start.year = "2021";
        c.rel_end.year = "2022";
        c.format_time = "%H";
        c.format_datetime = "%Y %H";
        c.format_date = "%Y";

        formatter f;
        f.fill_from_cfg(c);

        REQUIRE("%H" == f.format_time);
        REQUIRE("%Y" == f.format_date);
        REQUIRE("%Y %H" == f.format_datetime);

        REQUIRE("2021" == f.replacements["%PERIOD_START_DATE%"]);
        REQUIRE("00" == f.replacements["%PERIOD_START_TIME%"]);
        REQUIRE("2021 00" == f.replacements["%PERIOD_START_DATETIME%"]);

        REQUIRE("2022" == f.replacements["%PERIOD_END_DATE%"]);
        REQUIRE("23" == f.replacements["%PERIOD_END_TIME%"]);
        REQUIRE("2022 23" == f.replacements["%PERIOD_END_DATETIME%"]);
    }
}

// Copyright (C) 2021 Rolf Pfeffertal
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef __TERMINNANNY_FORMATTER_H_INCLUDED__
#define __TERMINNANNY_FORMATTER_H_INCLUDED__

#include <string>
#include <map>

#include <terminnanny/event.h>
#include <terminnanny/cfg.h>

using std::string;

namespace terminnanny {
    class formatter {
    public:
        std::map<string, string> replacements = {
            {"\\n", "\n"},
            {"\\\\", "\\"},
        };
        string format_date, format_time, format_datetime;

        /// substitute elems in string
        string get_replaced(const string&) const;
        /// in-place substitution in string
        void replace(string&) const;

        /// generate replacement from event. uses format_{date|time|datetime}
        void fill_from_event(const event&);
        /// sets general parameters from configuration
        void fill_from_cfg(const config&);

        /// removes leading/trailing whitespaces
        static string get_stripped(const string& s);
    };
}

#endif // __TERMINNANNY_FORMATTER_H_INCLUDED__

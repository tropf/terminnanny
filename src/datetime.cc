// Copyright (C) 2021 Rolf Pfeffertal
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#include <terminnanny/datetime.h>

#include <ctime>
#include <chrono>
#include <string>
#include <iomanip>
#include <sstream>
#include <regex>

#include <spdlog/spdlog.h>

using std::chrono::system_clock;
using std::chrono::time_point;
using std::chrono::seconds;
using std::string;
using std::stoi;

terminnanny::datetime::datetime() {
    timestamp_utc = system_clock::to_time_t(system_clock::now());
}

terminnanny::datetime::datetime(time_t given_timestamp_utc) {
    set_utc(given_timestamp_utc);
}

terminnanny::datetime::datetime(const tm& given_tm_utc) {
    set_utc(given_tm_utc);
}

void terminnanny::datetime::set_utc(const tm& given_tm_utc) {
    tm tm_copy = given_tm_utc;
    time_t maybe_timestamp = std::mktime(&tm_copy);
    if (-1 == maybe_timestamp) {
        spdlog::error("could not convert given time, keeping current value {}", timestamp_utc);
        return;
    }

    // note: mktime is by default *local*, so add offset for UTC
    timestamp_utc = maybe_timestamp + get_utc_offset_sec();
}

void terminnanny::datetime::set_utc(time_t given_timestamp_utc) {
    timestamp_utc = given_timestamp_utc;
}

void terminnanny::datetime::set_localtime(const tm& given_tm_localtime) {
    tm tm_copy = given_tm_localtime;
    time_t maybe_timestamp = std::mktime(&tm_copy);
    if (-1 == maybe_timestamp) {
        spdlog::error("could not convert given time, keeping current value {}", timestamp_utc);
        return;
    }

    // note: mktime is by default *local*, so add offset for UTC
    timestamp_utc = maybe_timestamp;
}

static bool rel_is_relative(const string& s) {
    const std::regex rel_regex("[+-]\\d+");
    return std::regex_match(s, rel_regex);
}

static string get_weekday(const tm& t) {
    std::stringstream ss;
    ss << std::put_time(&t, "%w");
    return ss.str();
}

static void apply_relative_dt_to_tm(tm& tm_new, const terminnanny::relative_datetime& rel, bool ceil_mode) {
    if (!rel.is_valid()) {
        spdlog::error("could not apply relative datetime, is invalid: {}", rel.get_validation_error().to_string());
        return;
    }

    // create copy
    const std::tm tm_current = tm_new;

    if (!rel.year.empty()) {
        // set year -> reset all less significant components
        tm_new.tm_mon = 0;
        tm_new.tm_mday = 1;
        tm_new.tm_hour = 0;
        tm_new.tm_min = 0;
        tm_new.tm_sec = 0;

        if (rel_is_relative(rel.year)) {
            tm_new.tm_year = tm_current.tm_year + stoi(rel.year);
        } else {
            // is absolute
            // 1900 is year 0
            tm_new.tm_year = stoi(rel.year) - 1900;
        }
    }

    if (!rel.month.empty()) {
        // reset all less significant components
        tm_new.tm_mday = 1;
        tm_new.tm_hour = 0;
        tm_new.tm_min = 0;
        tm_new.tm_sec = 0;
        
        if (rel_is_relative(rel.month)) {
            tm_new.tm_mon = tm_current.tm_mon + stoi(rel.month);
        } else {
            // january is 0
            tm_new.tm_mon = stoi(rel.month) - 1;
        }
    }

    if (!rel.week.empty()) {
        // reset less significant components
        tm_new.tm_hour = 0;
        tm_new.tm_min = 0;
        tm_new.tm_sec = 0;

        // only relative numbers allowed
        tm_new.tm_mday = tm_current.tm_mday + 7 * stoi(rel.week);

        // cycle to start of week
        string first_weekday = rel.week_starts_monday ? "1" : "0";
        while (first_weekday != get_weekday(tm_new)) {
            tm_new.tm_mday--;
            // re-normalize
            std::mktime(&tm_new);
        }

        // rel.day describes day relative to now calculated week
        if (!rel.day.empty()) {
            // can only be relative
            tm_new.tm_mday += stoi(rel.day);

            // re-normalize
            std::mktime(&tm_new);
        }
    } else {
        // rel.day describes day relative to current timestamp_utc
        if (!rel.day.empty()) {
            // reset less significant components
            tm_new.tm_hour = 0;
            tm_new.tm_min = 0;
            tm_new.tm_sec = 0;

            if (rel_is_relative(rel.day)) {
                tm_new.tm_mday = tm_current.tm_mday + stoi(rel.day);
            } else {
                tm_new.tm_mday = stoi(rel.day);
            }
        }
    }

    if (!rel.hour.empty()) {
        // reset all less significant components
        tm_new.tm_min = 0;
        tm_new.tm_sec = 0;
        
        if (rel_is_relative(rel.hour)) {
            tm_new.tm_hour = tm_current.tm_hour + stoi(rel.hour);
        } else {
            tm_new.tm_hour = stoi(rel.hour);
        }
    }

    if (!rel.minute.empty()) {
        // reset all less significant components
        tm_new.tm_sec = 0;
        
        if (rel_is_relative(rel.minute)) {
            tm_new.tm_min = tm_current.tm_min + stoi(rel.minute);
        } else {
            tm_new.tm_min = stoi(rel.minute);
        }
    }
    
    if (!rel.second.empty()) {
        if (rel_is_relative(rel.second)) {
            tm_new.tm_sec = tm_current.tm_sec + stoi(rel.second);
        } else {
            tm_new.tm_sec = stoi(rel.second);
        }
    }


    if (ceil_mode && !rel.is_empty()) {
        // normalize
        std::mktime(&tm_new);

        // set all components, that are less significant than the least significant specified component to their max
        if (!rel.second.empty()) {
            return;
        }
        tm_new.tm_sec = 59;

        if (!rel.minute.empty()) {
            return;
        }
        tm_new.tm_min = 59;

        if (!rel.hour.empty()) {
            return;
        }
        tm_new.tm_hour = 23;
        
        if (!rel.day.empty()) {
            return;
        }

        if (!rel.week.empty()) {
            // week specified -> set to end of week
            // cycle to end of week
            string first_weekday = rel.week_starts_monday ? "0" : "6";
            while (first_weekday != get_weekday(tm_new)) {
                tm_new.tm_mday++;
                // re-normalize
                std::mktime(&tm_new);
            }

            // do not tinker with the month etc further
            return;
        }

        // week not specified -> set to last day of month
        tm_new.tm_mon++;
        tm_new.tm_mday = 0;
        // normalize
        std::mktime(&tm_new);

        if (!rel.month.empty()) {
            return;
        }

        tm_new.tm_mon = 11;
    }

    // normalize
    std::mktime(&tm_new);
}

void terminnanny::datetime::apply_relative_datetime_utc(const terminnanny::relative_datetime& rel, bool ceil_mode) {
    std::tm tm_current = *std::gmtime(&timestamp_utc);
    apply_relative_dt_to_tm(tm_current, rel, ceil_mode);
    set_utc(tm_current);
}

void terminnanny::datetime::apply_relative_datetime_localtime(const terminnanny::relative_datetime& rel, bool ceil_mode) {
    std::tm tm_current = *std::localtime(&timestamp_utc);
    apply_relative_dt_to_tm(tm_current, rel, ceil_mode);
    set_localtime(tm_current);
}

time_t terminnanny::datetime::get_time_t_utc() const {
    return timestamp_utc;
}

tm terminnanny::datetime::get_tm_utc() const {
    return *std::gmtime(&timestamp_utc);
}

tm terminnanny::datetime::get_tm_localtime() const {
    return *std::localtime(&timestamp_utc);
}

string terminnanny::datetime::get_string_localtime(const string& format) const {
    std::stringstream ss;
    ss << std::put_time(std::localtime(&timestamp_utc), format.c_str());
    return ss.str();
}

string terminnanny::datetime::get_string_utc(const string& format) const {
    std::stringstream ss;
    ss << std::put_time(std::gmtime(&timestamp_utc), format.c_str());
    return ss.str();
}

time_t terminnanny::datetime::get_utc_offset_sec() {
    time_t now = system_clock::to_time_t(system_clock::now());

    time_t local = std::mktime(std::localtime(&now));
    time_t utc = std::mktime(std::gmtime(&now));
    return local - utc;
}

bool terminnanny::operator<(const terminnanny::datetime& lhs, const terminnanny::datetime& rhs) {
    return lhs.get_time_t_utc() < rhs.get_time_t_utc();
}

bool terminnanny::operator>(const terminnanny::datetime& lhs, const terminnanny::datetime& rhs) {
    return lhs.get_time_t_utc() > rhs.get_time_t_utc();
}

bool terminnanny::operator==(const terminnanny::datetime& lhs, const terminnanny::datetime& rhs) {
    return lhs.get_time_t_utc() == rhs.get_time_t_utc();
}

bool terminnanny::operator!=(const terminnanny::datetime& lhs, const terminnanny::datetime& rhs) {
    return lhs.get_time_t_utc() != rhs.get_time_t_utc();
}

bool terminnanny::operator<=(const terminnanny::datetime& lhs, const terminnanny::datetime& rhs) {
    return lhs.get_time_t_utc() <= rhs.get_time_t_utc();
}

bool terminnanny::operator>=(const terminnanny::datetime& lhs, const terminnanny::datetime& rhs) {
    return lhs.get_time_t_utc() >= rhs.get_time_t_utc();
}
